import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginViewComponent } from './login-view/login-view.component';
import { RegisterViewComponent } from './register-view/register-view.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from 'src/app/account/edit/edit.component';
import { DetailComponent } from 'src/app/account/detail/detail.component';

export const routing: ModuleWithProviders = RouterModule.forChild([
  { path: 'register', component: RegisterViewComponent },
  { path: 'login', component: LoginViewComponent }
]);
