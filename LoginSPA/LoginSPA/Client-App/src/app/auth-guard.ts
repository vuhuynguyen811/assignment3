import { UserService } from "./share/services/user.service";
import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private user: UserService, private router: Router, ) {

  }

  canActivate() {
    const token = localStorage.getItem('token');

    if (!this.user.isLoggedIn()) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
