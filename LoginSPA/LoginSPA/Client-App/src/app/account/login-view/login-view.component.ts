import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import decode from 'jwt-decode';
import { LoginForm } from '../../share/models/login-form';
import { UserService } from '../../share/services/user.service';
import { finalize } from 'rxjs/operators';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/internal/operators/catchError';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.scss']
})
export class LoginViewComponent implements OnInit {

  private subscription: Subscription;

  brandNew: boolean;
  errors: string;
  submitted: boolean = false;
  credentials: LoginForm = { email: '', password: '' };


  constructor(private userService: UserService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    // subscribe to router event
    this.subscription = this.activatedRoute.queryParams.subscribe(
      (param: any) => {
        this.brandNew = param['brandNew'];
        this.credentials.email = param['email'];
      });
  }

  ngOnDestroy() {
    // prevent memory leak by unsubscribing
    this.subscription.unsubscribe();
  }

  login({ value, valid }: { value: LoginForm, valid: boolean }) {
    this.submitted = true;
    this.errors = '';

    if (valid) {
      this.userService.login(value.email, value.password).pipe
        (map(response => response.json()))
        .subscribe(
        result => {

          localStorage.setItem('auth_token', result.auth_token);

          const tokenPayload = decode(result.auth_token);

          this.userService.setLoggedIn();

          this.userService.setauthNavStatus();

          this.router.navigate(['/dashboard/home']);                      
         },
        errors => { this.errors = this.userService.handleError(errors) });
    }
  }
}
