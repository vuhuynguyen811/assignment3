import { Injectable } from '@angular/core';
import decode from 'jwt-decode';
import { BaseService } from "./base.service";
import { ConfigService } from "./config.service";
import { UserViewModel } from "../models/registration-form";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { Observable, of } from 'rxjs';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { map, catchError } from 'rxjs/operators';
import { error } from '@angular/compiler/src/util';

@Injectable()
export class UserService extends BaseService {

  baseUrl: string = '';

  // Observable navItem source
  private _authNavStatusSource = new BehaviorSubject<boolean>(false);
  // Observable navItem stream
  authNavStatus$ = this._authNavStatusSource.asObservable();

  private loggedIn = false;
  private role: string;

  //
  constructor(private http: Http, private configService: ConfigService) {
    super();
    this.loggedIn = !!localStorage.getItem('auth_token');
    this.role = "customer";
    this._authNavStatusSource.next(this.loggedIn);
    this.baseUrl = configService.getApiURI();
  }

  //
  register(email: string, password: string, firstName: string, lastName: string, location: string, role: string): Observable<any> {
    
    let body = JSON.stringify({ email, password, firstName, lastName, location, role});
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.baseUrl + "/accounts/registration", body, options);
  }

  //
  createUser(email: string, password: string, firstName: string, role:string, lastName: string, location: string): Observable<any> {

    let body = JSON.stringify({ email, password, firstName, lastName, role, location });

    let headers = new Headers();
    headers = this.AddTokenToHeaders();

    return this.http.post(this.baseUrl + "/accounts/createUser", body, { headers });
  }

  //
  login(userName, password) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post( this.baseUrl + '/auth/login', JSON.stringify({ userName, password }), { headers });
  }

  //
  getDetail(email) {

    let headers = new Headers();
    headers = this.AddTokenToHeaders();

    return this.http.get(this.baseUrl + '/accounts/getDetail?userName=' + email, { headers });
  }

  Update(email, firstName, lastName) {
    let headers = new Headers();
    headers = this.AddTokenToHeaders();

    return this.http.put(this.baseUrl + '/accounts/update', JSON.stringify({ email, firstName, lastName }), { headers });
  }

  deleteUser(userName)
  {
    let headers = new Headers();
    headers = this.AddTokenToHeaders();

    return this.http.delete(this.baseUrl + '/accounts/delete?userName=' + userName, { headers });
  }

  logout() {
    localStorage.removeItem('auth_token');
    this.loggedIn = false;
    this._authNavStatusSource.next(false);
  }

  isLoggedIn() {
    return this.loggedIn;
  }

  AddTokenToHeaders(): Headers{
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let authToken = localStorage.getItem('auth_token');
    headers.append('Authorization', `Bearer ${authToken}`);

    return headers;
  }

  setLoggedIn() {
    this.loggedIn = true;
  }

  getRole(): string {

    return this.DecodeToken().Role;
  }

  setauthNavStatus() {
    this._authNavStatusSource.next(true);
  }

  DecodeToken(): any {
    const token = localStorage.getItem('auth_token');

    const tokenPayload = decode(token);

    return tokenPayload;
  }
}
