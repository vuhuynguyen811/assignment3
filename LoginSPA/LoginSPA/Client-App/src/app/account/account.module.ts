import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { routing } from './account.routing';
import { UserService } from '../share/services/user.service';
import { RegisterViewComponent } from './register-view/register-view.component';
import { LoginViewComponent } from './login-view/login-view.component';
import { EmailValidator } from '../directives/email.validator.directive';
import { ShareModule } from '../share/share.module';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { DetailComponent } from './detail/detail.component';


@NgModule({
  imports: [
    CommonModule, FormsModule, routing, ShareModule
  ],
  declarations: [RegisterViewComponent, EmailValidator, LoginViewComponent],
  providers: [UserService]
})

export class AccountModule { }
