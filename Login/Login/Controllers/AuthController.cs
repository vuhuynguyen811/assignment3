﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Login.Models;
using Login.Models.Entities;
using Login.Models.Jwt;
using Login.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;


namespace Login.Controllers
{
    [Produces("application/json")]
    [Route("api/Auth")]
    public class AuthController : Controller
    {
		private readonly UserManager<User> _userManager;
		private readonly IJwtFactory _jwtFactory;
		private readonly RoleManager<IdentityRole> _roleManager;
		private readonly JwtIssuerOptions _jwtOptions;

		public AuthController(UserManager<User> userManager, IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtOptions, RoleManager<IdentityRole> roleManager)
		{
			_userManager = userManager;
			_jwtFactory = jwtFactory;
			_jwtOptions = jwtOptions.Value;
			_roleManager = roleManager;
		}

        // Login | api/login
		[HttpPost("login")]
		public async Task<IActionResult> Login([FromBody]LoginDto credentials)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var identity = await GetClaimsIdentity(credentials.UserName, credentials.Password);

			if (identity == null)
			{
				return BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid username or password.", ModelState));
			}

			var response = new
			{
				id = identity.Claims.Single(c => c.Type == "id").Value,
                role =  identity.Claims.Single(c => c.Type == ClaimTypes.Role).Value,
				auth_token = await _jwtFactory.GenerateEncodedToken(credentials.UserName, identity),
				expires_in = (int)_jwtOptions.ValidFor.TotalSeconds
			};

			return Json(response);
		}

		private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
		{
			if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
				return await Task.FromResult<ClaimsIdentity>(null);

			// get the user to verifty
			var userToVerify = await _userManager.FindByNameAsync(userName);

			if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

			var role = await _userManager.GetRolesAsync(userToVerify);
			var roleName = role[0];

			// check the credentials
			if (await _userManager.CheckPasswordAsync(userToVerify, password))
			{
				return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userToVerify.UserName, userToVerify.Id, roleName));
			}

			// Credentials are invalid, or account doesn't exist
			return await Task.FromResult<ClaimsIdentity>(null);
		}
	}
}