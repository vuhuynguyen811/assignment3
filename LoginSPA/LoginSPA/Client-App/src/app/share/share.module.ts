import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { myFocus } from '../directives/focus.directive';


@NgModule({
  imports: [CommonModule],
  declarations: [myFocus ],
  exports: [myFocus],
  providers: []
  })

export class ShareModule { }

