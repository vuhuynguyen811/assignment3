import { Injectable } from '@angular/core';
import { BaseService } from '../share/services/base.service';
import { Http, Headers } from '@angular/http';
import { ConfigService } from '../share/services/config.service';
import { Observable } from 'rxjs';

  @Injectable()

  export class DashboardService extends BaseService {

  baseUrl: string = '';

  constructor(private http: Http, private configService: ConfigService) {
    super();
    this.baseUrl = configService.getApiURI();
  }

  getHomeDetails(): Observable <any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let authToken = localStorage.getItem('auth_token');
    headers.append('Authorization', `Bearer ${authToken}`);

    return this.http.get(this.baseUrl + "/dashboard/home", { headers });
  }
}
