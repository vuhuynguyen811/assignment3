﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Login.Models.Entities
{
	public class User : IdentityUser
	{
		// Extended Properties
		public string FirstName { get; set; }
		public string LastName { get; set; }
        public string Location { get; set; }
    }
}
