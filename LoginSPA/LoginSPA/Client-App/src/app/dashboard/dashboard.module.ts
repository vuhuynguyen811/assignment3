import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './dashboard.routing';
import { RootComponent } from './root/root.component';
import { HomeComponent } from './home/home.component';
import { SettingsComponent } from './settings/settings.component';
import { DashboardService } from './dashboard.service';
import { FormsModule } from '@angular/forms';
import { ShareModule } from 'src/app/share/share.module';
import { AuthGuard } from '../auth-guard';
import { PagerService } from 'src/app/share/services/pager.service';
import { CreateComponent } from 'src/app/account/create/create.component';
import { DetailComponent } from 'src/app/account/detail/detail.component';
import { EditComponent } from 'src/app/account/edit/edit.component';

import { ManagementComponent } from './management/management.component';
import { UserListComponent } from 'src/app/dashboard/management/user-list/user-list.component';
import { UserService } from 'src/app/share/services/user.service';
import { RoleGuard } from 'src/app/role-guard';
import { JwtHelperService } from '@auth0/angular-jwt';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    routing,
    ShareModule
  ],
  declarations: [RootComponent, HomeComponent, SettingsComponent, ManagementComponent, CreateComponent, EditComponent, DetailComponent, UserListComponent],
  exports: [],
  providers: [AuthGuard, DashboardService, PagerService, UserService, RoleGuard, JwtHelperService]
})
export class DashboardModule { }
