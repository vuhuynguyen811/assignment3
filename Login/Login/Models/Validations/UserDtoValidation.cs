﻿using FluentValidation;
using Login.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Login.Models.Validations
{
    public class UserDtoValidation : AbstractValidator<UserDto>
    {
		public UserDtoValidation()
		{
			RuleFor(vm => vm.Email).NotEmpty().WithMessage("Email cannot be empty");
			RuleFor(vm => vm.Password).NotEmpty().WithMessage("Password cannot be empty");
			RuleFor(vm => vm.FirstName).NotEmpty().WithMessage("FirstName cannot be empty");
			RuleFor(vm => vm.LastName).NotEmpty().WithMessage("LastName cannot be empty");
			RuleFor(vm => vm.Role).NotEmpty().WithMessage("Role cannot be empty");
			RuleFor(vm => vm.Location).NotEmpty().WithMessage("Location cannot be empty");
		}
    }
}
