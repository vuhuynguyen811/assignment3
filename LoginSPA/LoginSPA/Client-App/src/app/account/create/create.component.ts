import { Component, OnInit } from '@angular/core';
import { UserViewModel } from 'src/app/share/models/registration-form';
import { UserService } from 'src/app/share/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  errors: string;
  submitted: boolean = false;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  }


  createUser({ value, valid }: { value: UserViewModel, valid: boolean }) {
    this.submitted = true;
    this.errors = '';

    if (valid) {
      this.userService.createUser(value.email, value.password, value.firstName, value.role, value.lastName, value.location)
        .subscribe(
        result => {
          if (result) {
            this.router.navigate(['/dashboard/management']);
          }
        },
        errors => { this.errors = this.userService.handleError(errors) });
    }
  }  

}
