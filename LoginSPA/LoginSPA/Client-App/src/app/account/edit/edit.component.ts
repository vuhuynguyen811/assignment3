import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/share/services/user.service';
import { Router } from '@angular/router';
import { UserViewModel } from 'src/app/share/models/registration-form';
import { map } from 'rxjs/internal/operators/map';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  submitted: boolean = false;
  errors: string;
  private subscription: Subscription;

  public User: any = { email: '', firstName: '', lastName:'' };

  constructor(private activatedRoute: ActivatedRoute, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.queryParams.subscribe(
      (param: any) => {
        this.User.email = param['email'];
      });

    this.getUserDetail();

  }
  editUser({ value, valid }: { value: UserViewModel, valid: boolean }) {
    this.submitted = true;
    this.errors = '';

    if (valid) {
      this.userService.Update(this.User.email, value.firstName, value.lastName)
        .subscribe(
          result => {
            if (result) {
              this.router.navigate(['/dashboard/management']);
            }
          },
        errors => { this.errors = this.userService.handleError(errors) });
    }
  }  

  getUserDetail() {
    this.errors = '';

    this.userService.getDetail(this.User.email).pipe(map(res => res.json()))
      .subscribe(result => {
        this.User.firstName = result.firstName;
        this.User.lastName = result.lastName;
        this.User.location = result.location;
        this.User.email = result.email;
      },
      errors => this.errors = errors);
  }
}
