import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SettingsComponent } from './settings/settings.component';
import { RootComponent } from './root/root.component';
import { AuthGuard } from '../auth-guard';
import { ManagementComponent } from './management/management.component';
import { CreateComponent } from 'src/app/account/create/create.component';
import { DetailComponent } from 'src/app/account/detail/detail.component';
import { EditComponent } from 'src/app/account/edit/edit.component';
import { UserListComponent } from 'src/app/dashboard/management/user-list/user-list.component';
import { RoleGuard } from 'src/app/role-guard';



export const routing: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'dashboard',
    component: RootComponent, canActivate: [AuthGuard],

    children: [
      { path: '', component: HomeComponent },
      { path: 'home', component: HomeComponent },
      {
        path: 'management', component: ManagementComponent, canActivate: [RoleGuard], data: {expectedRole: 'Admin'} , children: [
          { path: '', component: UserListComponent },
          { path: 'create', component: CreateComponent },
          { path: 'detail', component: DetailComponent },
          { path: 'edit', component: EditComponent }
        ]
      },
      { path: 'settings', component: SettingsComponent },
    ]
  }
]);
