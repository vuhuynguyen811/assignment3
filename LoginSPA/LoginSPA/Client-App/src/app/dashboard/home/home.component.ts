import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  errors: string;

  public userDetails: any = {
    userName: '',
    firstName: '',
    lastName: '',
    location: ''
  };
  
  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.errors = '';

    this.dashboardService.getHomeDetails().pipe(map(response => response.json()))
      .subscribe(result => {
        this.userDetails.userName = result.email;
        this.userDetails.firstName = result.firstName;
        this.userDetails.lastName = result.lastName;
        this.userDetails.location = result.location;
      }, error => this.errors = error);
  }

}
