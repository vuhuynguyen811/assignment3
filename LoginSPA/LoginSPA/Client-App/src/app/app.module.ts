import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AccountModule } from './account/account.module';
import { HttpModule, XHRBackend } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ConfigService } from './share/services/config.service';
import { DashboardModule } from './dashboard/dashboard.module';
import { routing } from './app.routing';
import { AuthenticateXHRBackend } from 'src/app/authenticate-xhrbackend';
import { JwtHelperService } from '@auth0/angular-jwt';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent      
  ],
  imports: [
    AccountModule,
    DashboardModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [ConfigService, JwtHelperService, {
    provide: XHRBackend,
    useClass: AuthenticateXHRBackend
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
