﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Login.Models.Data;
using Login.Models.Entities;
using Login.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Login.Controllers
{
    [Authorize]
    [Route("api/Dashboard")]
    public class DashboardController : Controller
    {
        private readonly ClaimsPrincipal _caller;
		private readonly UserManager<User> _userManager;

		public DashboardController(UserManager<User> userManager, IHttpContextAccessor httpContextAccessor)
        {
            _caller = httpContextAccessor.HttpContext.User;
			_userManager = userManager;
        }

        // GET api/dashboard/home
        [Route("Home")]
        [HttpGet]
        public async Task<IActionResult> userProfile()
        {
            // retrieve the user info
            var userId = _caller.Claims.Single(c => c.Type == "id").Value;
		
            var userInDb = await _userManager.FindByIdAsync(userId);

			var userDto = new UserDto();

			Mapper.Map<User, UserDto>(userInDb, userDto);

			return Json(userDto);
        }
    }
}