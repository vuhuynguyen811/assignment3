﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Login.Models;
using Login.Models.Data;
using Login.Models.Entities;
using Login.Models.Pagination;
using Login.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Login.Controllers
{
	[Produces("application/json")]
	[Route("api/Accounts")]
	public class AccountsController : Controller
	{
		private readonly ApplicationDbContext _appDbContext;
		private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IMapper _mapper;
        public static readonly int pageSize = 5;

		public AccountsController(UserManager<User> userManager, IMapper mapper, ApplicationDbContext appDbContext, RoleManager<IdentityRole> roleManager)
		{
			_userManager = userManager;
			_mapper = mapper;
            _roleManager = roleManager;
            _appDbContext = appDbContext;
		}

        // Register Account| POST api/accounts/Registration
        [HttpPost]
		[Route("Registration")]
		public async Task<IActionResult> Registration([FromBody]UserDto userDto)
		{

			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
            
            //Create User
            var identityUser = _mapper.Map<User>(userDto);

			var createStatus = await _userManager.CreateAsync(identityUser, userDto.Password);

			if (!createStatus.Succeeded) return new BadRequestObjectResult(Errors.AddErrorsToModelState(createStatus, ModelState));

            //Create Role
            var staticRole = "customer";

            var createRoleStatus = await _roleManager.RoleExistsAsync(staticRole);

            if (createRoleStatus == false)
            {
                var role = new IdentityRole();
                role.Name = staticRole;
                await _roleManager.CreateAsync(role);
            }

            var addUserWithRoleStatus = await _userManager.AddToRoleAsync(identityUser, staticRole);
            if (!addUserWithRoleStatus.Succeeded) return new BadRequestObjectResult(Errors.AddErrorsToModelState(addUserWithRoleStatus, ModelState));

            _appDbContext.SaveChanges();
         
			return new OkObjectResult("Account created");
		}       

        // Get User List | api/Account/user?pageNumber=PageNumber
        [Authorize(Roles ="Admin")]
        [HttpGet]
        [Route("getUserList")]
        public IActionResult GetUserList(int pageNumber)
        {
            var userList = _appDbContext.Users.Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToList();
			var userDtoList = new List<UserDto>();

			foreach (User user in userList)
			{
				var userDto = new UserDto();
				Mapper.Map<User, UserDto>(user, userDto);
                userDtoList.Add(userDto);
			}

            var Totalpage = _appDbContext.Users.Count();
            var pagedData = Pagination.PagedResult(userDtoList, pageNumber, Totalpage, pageSize);

            return Json(pagedData);
        }

        // Create an user account | /api/accounts/createUser
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("createUser")]
        public async Task<IActionResult> CreateUser([FromBody]UserDto userDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //Create User
            var userIdentity = _mapper.Map<User>(userDto);

            var createUserStatus = await _userManager.CreateAsync(userIdentity, userDto.Password);

            if (!createUserStatus.Succeeded) return new BadRequestObjectResult(Errors.AddErrorsToModelState(createUserStatus, ModelState));

            //Create Role
            var createRoleStatus = await _roleManager.RoleExistsAsync(userDto.Role);

            if (createRoleStatus == false)
            {
                var role = new IdentityRole();
                role.Name = userDto.Role;
                await _roleManager.CreateAsync(role);
            }

            var addUserWithRoleStatus = await _userManager.AddToRoleAsync(userIdentity, userDto.Role);

            if (!addUserWithRoleStatus.Succeeded) return new BadRequestObjectResult(Errors.AddErrorsToModelState(addUserWithRoleStatus, ModelState));

            _appDbContext.SaveChanges();

            return new OkObjectResult("Account created");
        }

        // Update | api/Account/getDetail?userName=UserName
        [Authorize]
        [HttpGet]
        [Route("getDetail")]
        public async Task<IActionResult> GetUserDetail(string userName)
        {
            var result = await _userManager.FindByNameAsync(userName);
			var userDetail = new UserDto();

			Mapper.Map<User, UserDto>(result, userDetail);

            userDetail.Password = "Nothing";

            return Ok(userDetail);
        }

        // Update | api/Account/update?id=Id
        [Authorize(Roles = "Admin")]
        [HttpPut]
		[Route("update")]
		public async Task<IActionResult> UpdateUserDetail([FromBody]UserDto userDto)
		{
			var userInDb = await _userManager.FindByNameAsync(userDto.Email);

            userInDb.FirstName = userDto.FirstName;
            userInDb.LastName = userDto.LastName;

			await _userManager.UpdateAsync(userInDb);

			_appDbContext.SaveChanges();

			return Ok();
		}

        // Detele | api/Account/delete?id=Id
        [Authorize(Roles = "Admin")]
        [HttpDelete]
        [Route("delete")]
        public IActionResult DeleteUserr(string userName)
        {

            var userInDb = _appDbContext.Users.Single(c => c.UserName == userName);

            _appDbContext.Users.Remove(userInDb);
            _appDbContext.SaveChanges();

            return Ok();
        }

    }
}