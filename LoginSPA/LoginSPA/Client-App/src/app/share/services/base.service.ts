import { Observable } from 'rxjs/';


export abstract class BaseService {

  constructor() { }

   public handleError(error: any) {
    var applicationError = error.headers.get('Application-Error');

    if (applicationError) {
      return applicationError;
    }

    var modelStateErrors: string = '';
    var serverError = error.json();

    if (!serverError.type) {
      for (var key in serverError) {
        if (serverError[key])
          modelStateErrors += serverError[key] + '\n';
      }
    }
 
    modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
    return (modelStateErrors || 'Server error');
  }
}
