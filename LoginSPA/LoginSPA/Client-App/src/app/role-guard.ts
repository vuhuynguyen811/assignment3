import { UserService } from "./share/services/user.service";
import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { AuthGuard } from "src/app/auth-guard";
import { ActivatedRouteSnapshot } from "@angular/router";
import { RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import decode from 'jwt-decode';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private auth: AuthGuard, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {

    const expectedRole = route.data.expectedRole;

    const token = localStorage.getItem('auth_token');

    const tokenPayload = decode(token);

    if (!this.auth.canActivate() || tokenPayload.Role != expectedRole) {
      this.router.navigate(['/dashboard/home']);
      return false;
    }
    return true;
  }
}
