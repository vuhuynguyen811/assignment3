(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/account/account.module.ts":
/*!*******************************************!*\
  !*** ./src/app/account/account.module.ts ***!
  \*******************************************/
/*! exports provided: AccountModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountModule", function() { return AccountModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _account_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./account.routing */ "./src/app/account/account.routing.ts");
/* harmony import */ var _share_services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../share/services/user.service */ "./src/app/share/services/user.service.ts");
/* harmony import */ var _register_view_register_view_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./register-view/register-view.component */ "./src/app/account/register-view/register-view.component.ts");
/* harmony import */ var _login_view_login_view_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login-view/login-view.component */ "./src/app/account/login-view/login-view.component.ts");
/* harmony import */ var _directives_email_validator_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../directives/email.validator.directive */ "./src/app/directives/email.validator.directive.ts");
/* harmony import */ var _share_share_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../share/share.module */ "./src/app/share/share.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AccountModule = /** @class */ (function () {
    function AccountModule() {
    }
    AccountModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _account_routing__WEBPACK_IMPORTED_MODULE_3__["routing"], _share_share_module__WEBPACK_IMPORTED_MODULE_8__["ShareModule"]
            ],
            declarations: [_register_view_register_view_component__WEBPACK_IMPORTED_MODULE_5__["RegisterViewComponent"], _directives_email_validator_directive__WEBPACK_IMPORTED_MODULE_7__["EmailValidator"], _login_view_login_view_component__WEBPACK_IMPORTED_MODULE_6__["LoginViewComponent"]],
            providers: [_share_services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]]
        })
    ], AccountModule);
    return AccountModule;
}());



/***/ }),

/***/ "./src/app/account/account.routing.ts":
/*!********************************************!*\
  !*** ./src/app/account/account.routing.ts ***!
  \********************************************/
/*! exports provided: routing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routing", function() { return routing; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_view_login_view_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login-view/login-view.component */ "./src/app/account/login-view/login-view.component.ts");
/* harmony import */ var _register_view_register_view_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./register-view/register-view.component */ "./src/app/account/register-view/register-view.component.ts");



var routing = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild([
    { path: 'register', component: _register_view_register_view_component__WEBPACK_IMPORTED_MODULE_2__["RegisterViewComponent"] },
    { path: 'login', component: _login_view_login_view_component__WEBPACK_IMPORTED_MODULE_1__["LoginViewComponent"] }
]);


/***/ }),

/***/ "./src/app/account/create/create.component.html":
/*!******************************************************!*\
  !*** ./src/app/account/create/create.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div>\r\n  <div class=\"text-center\">\r\n    <h1>User Management</h1>\r\n  </div>\r\n\r\n  <div class=\"col-md-6\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header bg-white\">\r\n          <h4 class=\"card-title\">Create An New User</h4>\r\n        </div>\r\n\r\n        <div class=\"card-block\">\r\n          <form #form=\"ngForm\" novalidate (ngSubmit)=\"createUser(form)\">\r\n            <div class=\"form-group\">\r\n              <label for=\"first-name\"> First name</label>\r\n              <input id=\"first-name\" type=\"text\" required name=\"firstName\" class=\"form-control\" placeholder=\"Your first name\" tmFocus ngModel>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"last-name\"> Last name</label>\r\n              <input id=\"last-name\" type=\"text\" required name=\"lastName\" class=\"form-control\" placeholder=\"Your last name\" ngModel>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"email\"> Email</label>\r\n              <input id=\"email\" type=\"text\" required name=\"email\" validateEmail class=\"form-control\" placeholder=\"Email\" ngModel #email=\"ngModel\">\r\n              <small [hidden]=\"email.valid || (email.pristine && !submitted)\" class=\"text-danger\">Please enter a valid email</small>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"password\"> Password</label>\r\n              <input id=\"password\" type=\"password\" required name=\"password\" class=\"form-control\" placeholder=\"Password\" ngModel>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"role\"> Role</label>\r\n              <input id=\"role\" type=\"text\" required name=\"role\" class=\"form-control\" placeholder=\"Role\" ngModel>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"location\"> Location</label>\r\n              <input id=\"location\" type=\"text\" required name=\"location\" class=\"form-control\" placeholder=\"Location\" ngModel>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"form.invalid\"> Sign Up</button>\r\n              <button type=\"reset\" class=\"btn btn-primary\" routerLinkActive=\"active\" routerLink=\"/dashboard/management\">Cancel</button>\r\n            </div>\r\n\r\n            <div *ngIf=\"errors\" class=\"alert alert-danger\" role=\"alert\">\r\n              <strong>Oops!</strong> {{errors}}\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/account/create/create.component.scss":
/*!******************************************************!*\
  !*** ./src/app/account/create/create.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/account/create/create.component.ts":
/*!****************************************************!*\
  !*** ./src/app/account/create/create.component.ts ***!
  \****************************************************/
/*! exports provided: CreateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateComponent", function() { return CreateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/share/services/user.service */ "./src/app/share/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CreateComponent = /** @class */ (function () {
    function CreateComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.submitted = false;
    }
    CreateComponent.prototype.ngOnInit = function () {
    };
    CreateComponent.prototype.createUser = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        this.submitted = true;
        this.errors = '';
        if (valid) {
            this.userService.createUser(value.email, value.password, value.firstName, value.role, value.lastName, value.location)
                .subscribe(function (result) {
                if (result) {
                    _this.router.navigate(['/dashboard/management']);
                }
            }, function (errors) { _this.errors = _this.userService.handleError(errors); });
        }
    };
    CreateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create',
            template: __webpack_require__(/*! ./create.component.html */ "./src/app/account/create/create.component.html"),
            styles: [__webpack_require__(/*! ./create.component.scss */ "./src/app/account/create/create.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], CreateComponent);
    return CreateComponent;
}());



/***/ }),

/***/ "./src/app/account/detail/detail.component.html":
/*!******************************************************!*\
  !*** ./src/app/account/detail/detail.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header bg-white\">\r\n          <h4 class=\"card-title\">Update User Account Detail</h4>\r\n        </div>\r\n        <div class=\"card-block\">\r\n          <form #form=\"ngForm\" novalidate (ngSubmit)=\"registerUser(form)\">\r\n            <div class=\"form-group\">\r\n              <label for=\"email\">Email</label>\r\n              <input id=\"email\" [disabled]=\"true\" type=\"text\" [(ngModel)]=\"User.email\" name=\"email\" class=\"form-control\">\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"first-name\">First name</label>\r\n              <input id=\"first-name\" [disabled]=\"true\" type=\"text\" [(ngModel)]=\"User.firstName\" required name=\"firstName\" class=\"form-control\" placeholder=\"Your first name\" tmFocus>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"last-name\">Last name</label>\r\n              <input id=\"last-name\" [disabled]=\"true\" type=\"text\" [(ngModel)]=\"User.lastName\" required name=\"lastName\" class=\"form-control\" placeholder=\"Your last name\">\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <button type=\"submit\" class=\"btn btn-primary\" routerLinkActive=\"active\" routerLink=\"/dashboard/management\">Back</button>\r\n            </div>\r\n\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>-->\r\n\r\n<div class=\"container\">\r\n  <div class=\"text-center\"><h1>User Management</h1></div>\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad\">\r\n      <div class=\"panel panel-info\">\r\n        <div class=\"panel-heading\">\r\n          <h3 class=\"panel-title\">{{User.firstName}} {{User.lastName}}</h3>\r\n        </div>\r\n        <div class=\"panel-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-3 col-lg-3 \" align=\"center\"> <img alt=\"User Pic\" src=\"https://image.flaticon.com/icons/svg/3/3641.svg\" class=\"img-circle\"> </div>\r\n            <div class=\" col-md-9 col-lg-9 \">\r\n\r\n              <table class=\"table table-user-information \">\r\n                <tbody>\r\n                  <tr>\r\n                    <td>Department:</td>\r\n                    <td>Programming</td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>Hire date:</td>\r\n                    <td>06/23/2013</td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>Date of Birth</td>\r\n                    <td>01/24/1988</td>\r\n                  </tr>\r\n\r\n                  <tr>\r\n                    <td>Gender</td>\r\n                    <td>Male</td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>Home Address</td>\r\n                    <td>{{User.location}}</td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>Email</td>\r\n                    <td><a href=\"#\">{{User.email}}</a></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>Phone Number</td>\r\n                    <td>\r\n                      123-4567-890(Landline)<br><br>555-4567-890(Mobile)\r\n                    </td>\r\n                </tbody>\r\n              </table>\r\n              <div class=\"row justify-content-center\">\r\n                <!--<a href=\"#\" class=\"btn btn-primary btn-space\">Edit User</a>-->\r\n                <a href=\"#\" class=\"btn btn-primary btn-space\" routerLinkActive=\"active\" routerLink=\"/dashboard/management/\">Back</a>\r\n              </div>\r\n            </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/account/detail/detail.component.scss":
/*!******************************************************!*\
  !*** ./src/app/account/detail/detail.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".user-row {\n  margin-bottom: 14px; }\n\n.user-row:last-child {\n  margin-bottom: 0; }\n\n.dropdown-user {\n  margin: 13px 0;\n  padding: 5px;\n  height: 100%; }\n\n.dropdown-user:hover {\n  cursor: pointer; }\n\n.table-user-information > tbody > tr {\n  border-top: 1px solid #dddddd; }\n\n.table-user-information > tbody > tr:first-child {\n  border-top: 0; }\n\n.table-user-information > tbody > tr > td {\n  border-top: 0; }\n\n.toppad {\n  margin-top: 20px; }\n\n.btn-space {\n  margin-right: 5px; }\n"

/***/ }),

/***/ "./src/app/account/detail/detail.component.ts":
/*!****************************************************!*\
  !*** ./src/app/account/detail/detail.component.ts ***!
  \****************************************************/
/*! exports provided: DetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailComponent", function() { return DetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/share/services/user.service */ "./src/app/share/services/user.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DetailComponent = /** @class */ (function () {
    function DetailComponent(activatedRoute, userService) {
        this.activatedRoute = activatedRoute;
        this.userService = userService;
        this.submitted = false;
        this.User = { email: '', firstName: '', lastName: '', location: '' };
    }
    DetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.activatedRoute.queryParams.subscribe(function (param) {
            _this.User.email = param['email'];
        });
        this.getUserDetail();
    };
    DetailComponent.prototype.getUserDetail = function () {
        var _this = this;
        this.errors = '';
        this.userService.getDetail(this.User.email).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res.json(); }))
            .subscribe(function (result) {
            _this.User.firstName = result.firstName;
            _this.User.lastName = result.lastName;
            _this.User.location = result.location;
            _this.User.email = result.email;
        }, function (errors) { return _this.errors = errors; });
    };
    DetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-detail',
            template: __webpack_require__(/*! ./detail.component.html */ "./src/app/account/detail/detail.component.html"),
            styles: [__webpack_require__(/*! ./detail.component.scss */ "./src/app/account/detail/detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]])
    ], DetailComponent);
    return DetailComponent;
}());



/***/ }),

/***/ "./src/app/account/edit/edit.component.html":
/*!**************************************************!*\
  !*** ./src/app/account/edit/edit.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container bootstrap snippet\">\r\n  <h1 class=\"text-center\">User Management</h1>\r\n  <div class=\"row ng-scope\">\r\n    <div class=\"col-md-4\">\r\n      <div class=\"panel panel-default\">\r\n        <div class=\"panel-body text-center\">\r\n          <div class=\"pv-lg\"><img class=\"center-block img-responsive img-circle img-thumbnail thumb96\" src=\"https://cdn1.iconfinder.com/data/icons/ninja-things-1/1772/ninja-128.png\" alt=\"Contact\"></div>\r\n          <h3 class=\"m0 text-bold\">{{User.firstName}} {{User.lastName}}</h3>\r\n          <div class=\"mv-lg\">\r\n            <p>Hello</p>\r\n          </div>\r\n          <div class=\"text-center\"><a class=\"btn btn-primary\" href=\"\">Send message</a></div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-8\">\r\n      <div class=\"panel panel-default\">\r\n        <div class=\"panel-body\">       \r\n          <div class=\"h4 text-center\">User Information</div>\r\n          <div class=\"row pv-lg\">\r\n            <div class=\"col-lg-2\"></div>\r\n            <div class=\"col-lg-8\">\r\n              <form class=\"form-horizontal ng-pristine ng-valid\" #form=\"ngForm\" (ngSubmit)=\"editUser(form)\">\r\n                <!-- firstName input// -->\r\n                <div class=\"form-group\">\r\n                  <label class=\"col-sm-2 control-label\" for=\"first-name\">FirstName</label>\r\n                  <div class=\"col-sm-10\">\r\n                    <input id=\"first-name\" type=\"text\" [(ngModel)]=\"User.firstName\" required name=\"firstName\" class=\"form-control\" placeholder=\"First name\" tmFocus #firstName=\"ngModel\" minlength=\"3\">\r\n                  </div>\r\n                </div>\r\n\r\n                <!-- firstName validate// -->\r\n                <div *ngIf=\"firstName.invalid && (firstName.dirty || firstName.touched)\" class=\"col-sm-10\">\r\n                  <small *ngIf=\"firstName.errors.required\" style=\"color:red\">\r\n                    First Name is required.\r\n                  </small>\r\n                  <small *ngIf=\"firstName.errors.minlength\" style=\"color:red\">\r\n                    First Name is at least 3 character\r\n                  </small>\r\n                </div><!-- form-group// -->\r\n                <!-- lastName input// -->\r\n                <div class=\"form-group\">\r\n                  <label class=\"col-sm-2 control-label\" for=\"last-name\">LastName</label>\r\n                  <div class=\"col-sm-10\">\r\n                    <input id=\"last-name\" type=\"text\" [(ngModel)]=\"User.lastName\" required name=\"lastName\" class=\"form-control\" placeholder=\"Last name\" #lastName=\"ngModel\" minlength=\"3\">\r\n                  </div>\r\n                </div>\r\n\r\n                <!-- lastName validate// -->\r\n                <div *ngIf=\"lastName.invalid && (lastName.dirty || lastName.touched)\" class=\"col-sm-10\">\r\n                  <small *ngIf=\"lastName.errors.required\" style=\"color:red\">\r\n                    Last Name is required.\r\n                  </small>\r\n                  <small *ngIf=\"lastName.errors.minlength\" style=\"color:red\">\r\n                    Last Name is at least 3 character\r\n                  </small>\r\n                </div><!-- form-group// -->\r\n                <!-- Email input// -->\r\n                <div class=\"form-group\">\r\n                  <label class=\"col-sm-2 control-label\" for=\"email\">Email</label>\r\n                  <div class=\"col-sm-10\">\r\n                    <input id=\"email\" [disabled]=\"true\" type=\"text\" [(ngModel)]=\"User.email\" validateEmail name=\"email\" class=\"form-control\" #email=\"ngModel\">\r\n                  </div>\r\n                </div>\r\n                <!-- Email Validate// -->\r\n                <div>\r\n                  <small [hidden]=\"email.valid || (email.pristine && !submitted)\" class=\"text-danger col-sm-10\">Please enter a valid email</small>\r\n                </div><!-- form-group// -->\r\n\r\n                <div class=\"form-group\">\r\n                  <label class=\"col-sm-2 control-label\" for=\"inputContact3\">Phone</label>\r\n                  <div class=\"col-sm-10\">\r\n                    <input class=\"form-control\" id=\"inputContact3\" type=\"text\" value=\"(123) 465 789\">\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                  <label class=\"col-sm-2 control-label\" for=\"inputContact5\">Website</label>\r\n                  <div class=\"col-sm-10\">\r\n                    <input class=\"form-control\" id=\"inputContact5\" type=\"text\" value=\"http://some.wesbite.com\">\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                  <label class=\"col-sm-2 control-label\" for=\"location\">Location</label>\r\n                  <div class=\"col-sm-10\">\r\n                    <input id=\"location\" type=\"text\" [(ngModel)]=\"User.location\" required name=\"location\" class=\"form-control\" placeholder=\"Location\">\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                  <div class=\"col-sm-offset-2 col-sm-10\">\r\n                    <button type=\"submit\" class=\"btn  btn-info btn-space\" [disabled]=\"form.invalid\">Save</button>\r\n                    <button type=\"reset\" class=\"btn btn-primary btn-space\" routerLinkActive=\"active\" routerLink=\"/dashboard/management\">Cancel</button>\r\n\r\n                  </div>\r\n                </div>\r\n                <div *ngIf=\"errors\" class=\"alert alert-danger\" role=\"alert\">\r\n                  <strong>Oops!</strong> {{errors}}\r\n                </div>\r\n              </form>\r\n             \r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/account/edit/edit.component.scss":
/*!**************************************************!*\
  !*** ./src/app/account/edit/edit.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-space {\n  margin-right: 5px; }\n\n.inner_div {\n  margin: 20px 5px; }\n\nleft {\n  text-align: left !important; }\n\nbody {\n  margin-top: 20px;\n  background: #f5f7fa; }\n\n.panel.panel-default {\n  border-top-width: 3px; }\n\n.panel {\n  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.14), 0 2px 2px 0 rgba(0, 0, 0, 0.098), 0 1px 5px 0 rgba(0, 0, 0, 0.084);\n  border: 0;\n  border-radius: 4px;\n  margin-bottom: 16px; }\n\n.thumb96 {\n  width: 96px !important;\n  height: 96px !important; }\n\n.thumb48 {\n  width: 48px !important;\n  height: 48px !important; }\n"

/***/ }),

/***/ "./src/app/account/edit/edit.component.ts":
/*!************************************************!*\
  !*** ./src/app/account/edit/edit.component.ts ***!
  \************************************************/
/*! exports provided: EditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditComponent", function() { return EditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/share/services/user.service */ "./src/app/share/services/user.service.ts");
/* harmony import */ var rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/internal/operators/map */ "./node_modules/rxjs/internal/operators/map.js");
/* harmony import */ var rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EditComponent = /** @class */ (function () {
    function EditComponent(activatedRoute, userService, router) {
        this.activatedRoute = activatedRoute;
        this.userService = userService;
        this.router = router;
        this.submitted = false;
        this.User = { email: '', firstName: '', lastName: '' };
    }
    EditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.activatedRoute.queryParams.subscribe(function (param) {
            _this.User.email = param['email'];
        });
        this.getUserDetail();
    };
    EditComponent.prototype.editUser = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        this.submitted = true;
        this.errors = '';
        if (valid) {
            this.userService.Update(this.User.email, value.firstName, value.lastName)
                .subscribe(function (result) {
                if (result) {
                    _this.router.navigate(['/dashboard/management']);
                }
            }, function (errors) { _this.errors = _this.userService.handleError(errors); });
        }
    };
    EditComponent.prototype.getUserDetail = function () {
        var _this = this;
        this.errors = '';
        this.userService.getDetail(this.User.email).pipe(Object(rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res.json(); }))
            .subscribe(function (result) {
            _this.User.firstName = result.firstName;
            _this.User.lastName = result.lastName;
            _this.User.location = result.location;
            _this.User.email = result.email;
        }, function (errors) { return _this.errors = errors; });
    };
    EditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__(/*! ./edit.component.html */ "./src/app/account/edit/edit.component.html"),
            styles: [__webpack_require__(/*! ./edit.component.scss */ "./src/app/account/edit/edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], EditComponent);
    return EditComponent;
}());



/***/ }),

/***/ "./src/app/account/login-view/login-view.component.html":
/*!**************************************************************!*\
  !*** ./src/app/account/login-view/login-view.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--div class=\"row\">\r\n  <div class=\"col-md-6 new-user-alert\">\r\n    <div *ngIf=\"brandNew\" class=\"alert alert-success\" role=\"alert\">\r\n      <strong>All set!</strong> Please login with your account\r\n    </div>\r\n    <h2>Login</h2>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-md-6\">\r\n    <form #form=\"ngForm\" novalidate (ngSubmit)=\"login(form)\">\r\n\r\n      <div class=\"form-group\">\r\n        <label for=\"email\">Email</label>\r\n        <input id=\"email\" type=\"text\" required name=\"email\" class=\"form-control\" placeholder=\"Email\" [ngModel]=\"credentials.email\" #email=\"ngModel\" tmFocus validateEmail>\r\n        <small [hidden]=\"email.valid || (email.pristine && !submitted)\" class=\"text-danger\">Please enter a valid email</small>\r\n      </div>\r\n\r\n      <div class=\"form-group\">\r\n        <label for=\"password\">Password</label>\r\n        <input type=\"password\" class=\"form-control\" id=\"password\" required name=\"password\" placeholder=\"Password\" ngModel>\r\n      </div>\r\n\r\n      <div class=\"form-group\">\r\n        <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"form.invalid\">Login</button>\r\n      </div>\r\n\r\n      <div *ngIf=\"errors\" class=\"alert alert-danger\" role=\"alert\">\r\n        <strong>Oops!</strong> {{errors}}\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>--><div class=\"wrapper fadeInDown\">\r\n  <div id=\"formContent\">\r\n    <!-- Tabs Titles -->\r\n    <h2 class=\"active\"> Sign In </h2>\r\n    <h2 class=\"inactive underlineHover\" routerLinkActive=\"active\" routerLink=\"/register\">Sign Up </h2>\r\n\r\n    <!-- Icon -->\r\n    <div class=\"fadeIn first\">\r\n      <img src=\"http://danielzawadzki.com/codepen/01/icon.svg\" id=\"icon\" alt=\"User Icon\" />\r\n    </div>\r\n\r\n    <!-- Login Form -->\r\n    <form #form=\"ngForm\" novalidate (ngSubmit)=\"login(form)\">\r\n\r\n      <div class=\"fadeIn second\">\r\n        <input id=\"email\" type=\"text\" required name=\"email\" placeholder=\"Email\" [ngModel]=\"credentials.email\" #email=\"ngModel\" tmFocus validateEmail>\r\n      </div>\r\n      <div> <small [hidden]=\"email.valid || (email.pristine && !submitted)\" class=\"text-danger\">Please enter a valid email</small></div>\r\n\r\n      <div class=\"fadeIn third \">\r\n        <input type=\"password\" class=\"\" id=\"password\" required name=\"password\" placeholder=\"Password\" ngModel>\r\n      </div>\r\n\r\n      <div class=\"form-group .fadeIn fourth \">\r\n        <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"form.invalid\">Login</button>\r\n      </div>\r\n\r\n      <div *ngIf=\"errors\" class=\"alert alert-danger\" role=\"alert\">\r\n        <strong>Oops!</strong> {{errors}}\r\n      </div>\r\n    </form>\r\n\r\n    <!-- Remind Passowrd -->\r\n    <div id=\"formFooter\">\r\n      <a class=\"underlineHover\" href=\"#\">Forgot Password?</a>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/account/login-view/login-view.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/account/login-view/login-view.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import url(\"https://fonts.googleapis.com/css?family=Poppins\");\n/* BASIC */\nhtml {\n  background-color: #56baed; }\nbody {\n  font-family: \"Poppins\", sans-serif;\n  height: 100vh; }\na {\n  color: #92badd;\n  display: inline-block;\n  text-decoration: none;\n  font-weight: 400; }\nh2 {\n  text-align: center;\n  font-size: 16px;\n  font-weight: 600;\n  text-transform: uppercase;\n  display: inline-block;\n  margin: 40px 8px 10px 8px;\n  color: #cccccc; }\n/* STRUCTURE */\n.wrapper {\n  display: flex;\n  align-items: center;\n  flex-direction: column;\n  justify-content: center;\n  width: 100%;\n  min-height: 100%;\n  padding: 20px; }\n#formContent {\n  border-radius: 10px 10px 10px 10px;\n  background: #fff;\n  padding: 30px;\n  width: 90%;\n  max-width: 450px;\n  position: relative;\n  padding: 0px;\n  box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);\n  text-align: center; }\n#formFooter {\n  background-color: #f6f6f6;\n  border-top: 1px solid #dce8f1;\n  padding: 25px;\n  text-align: center;\n  border-radius: 0 0 10px 10px; }\n/* TABS */\nh2.inactive {\n  color: #cccccc; }\nh2.active {\n  color: #0d0d0d;\n  border-bottom: 2px solid #5fbae9; }\n/* FORM TYPOGRAPHY*/\ninput[type=button], input[type=submit], input[type=reset] {\n  background-color: #56baed;\n  border: none;\n  color: white;\n  padding: 15px 80px;\n  text-align: center;\n  text-decoration: none;\n  display: inline-block;\n  text-transform: uppercase;\n  font-size: 13px;\n  box-shadow: 0 10px 30px 0 rgba(95, 186, 233, 0.4);\n  border-radius: 5px 5px 5px 5px;\n  margin: 5px 20px 40px 20px;\n  transition: all 0.3s ease-in-out; }\ninput[type=button]:hover, input[type=submit]:hover, input[type=reset]:hover {\n  background-color: #39ace7; }\ninput[type=button]:active, input[type=submit]:active, input[type=reset]:active {\n  -webkit-transform: scale(0.95);\n  transform: scale(0.95); }\ninput[type=text] {\n  background-color: #f6f6f6;\n  border: none;\n  color: #0d0d0d;\n  padding: 15px 32px;\n  text-align: center;\n  text-decoration: none;\n  display: inline-block;\n  font-size: 16px;\n  margin: 5px;\n  width: 85%;\n  border: 2px solid #f6f6f6;\n  transition: all 0.5s ease-in-out;\n  border-radius: 5px 5px 5px 5px; }\ninput[type=password] {\n  background-color: #f6f6f6;\n  border: none;\n  color: #0d0d0d;\n  padding: 15px 32px;\n  text-align: center;\n  text-decoration: none;\n  display: inline-block;\n  font-size: 16px;\n  margin: 5px;\n  width: 85%;\n  border: 2px solid #f6f6f6;\n  transition: all 0.5s ease-in-out;\n  border-radius: 5px 5px 5px 5px;\n  -webkit-text-security: disc; }\ninput[type=text]:focus {\n  background-color: #fff;\n  border-bottom: 2px solid #5fbae9; }\ninput[type=text]:placeholder {\n  color: #cccccc; }\n/* ANIMATIONS */\n/* Simple CSS3 Fade-in-down Animation */\n.fadeInDown {\n  -webkit-animation-name: fadeInDown;\n  animation-name: fadeInDown;\n  -webkit-animation-duration: 1s;\n  animation-duration: 1s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both; }\n@-webkit-keyframes fadeInDown {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0); }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n@keyframes fadeInDown {\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0); }\n  100% {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n/* Simple CSS3 Fade-in Animation */\n@-webkit-keyframes fadeIn {\n  from {\n    opacity: 0; }\n  to {\n    opacity: 1; } }\n@keyframes fadeIn {\n  from {\n    opacity: 0; }\n  to {\n    opacity: 1; } }\n.fadeIn {\n  opacity: 0;\n  -webkit-animation: fadeIn ease-in 1;\n  animation: fadeIn ease-in 1;\n  -webkit-animation-fill-mode: forwards;\n  animation-fill-mode: forwards;\n  -webkit-animation-duration: 1s;\n  animation-duration: 1s; }\n.fadeIn.first {\n  -webkit-animation-delay: 0.4s;\n  animation-delay: 0.4s; }\n.fadeIn.second {\n  -webkit-animation-delay: 0.6s;\n  animation-delay: 0.6s; }\n.fadeIn.third {\n  -webkit-animation-delay: 0.8s;\n  animation-delay: 0.8s; }\n.fadeIn.fourth {\n  -webkit-animation-delay: 1s;\n  animation-delay: 1s; }\n/* Simple CSS3 Fade-in Animation */\n.underlineHover:after {\n  display: block;\n  left: 0;\n  bottom: -10px;\n  width: 0;\n  height: 2px;\n  background-color: #56baed;\n  content: \"\";\n  transition: width 0.2s; }\n.underlineHover:hover {\n  color: #0d0d0d; }\n.underlineHover:hover:after {\n  width: 100%; }\n/* OTHERS */\n*:focus {\n  outline: none; }\n#icon {\n  width: 60%; }\n* {\n  box-sizing: border-box; }\n"

/***/ }),

/***/ "./src/app/account/login-view/login-view.component.ts":
/*!************************************************************!*\
  !*** ./src/app/account/login-view/login-view.component.ts ***!
  \************************************************************/
/*! exports provided: LoginViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginViewComponent", function() { return LoginViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _share_services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../share/services/user.service */ "./src/app/share/services/user.service.ts");
/* harmony import */ var rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/internal/operators/map */ "./node_modules/rxjs/internal/operators/map.js");
/* harmony import */ var rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginViewComponent = /** @class */ (function () {
    function LoginViewComponent(userService, router, activatedRoute) {
        this.userService = userService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.submitted = false;
        this.credentials = { email: '', password: '' };
    }
    LoginViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        // subscribe to router event
        this.subscription = this.activatedRoute.queryParams.subscribe(function (param) {
            _this.brandNew = param['brandNew'];
            _this.credentials.email = param['email'];
        });
    };
    LoginViewComponent.prototype.ngOnDestroy = function () {
        // prevent memory leak by unsubscribing
        this.subscription.unsubscribe();
    };
    LoginViewComponent.prototype.login = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        this.submitted = true;
        this.errors = '';
        if (valid) {
            this.userService.login(value.email, value.password).pipe(Object(rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) { return response.json(); }))
                .subscribe(function (result) {
                localStorage.setItem('auth_token', result.auth_token);
                var tokenPayload = jwt_decode__WEBPACK_IMPORTED_MODULE_2___default()(result.auth_token);
                _this.userService.setLoggedIn();
                _this.userService.setauthNavStatus();
                _this.router.navigate(['/dashboard/home']);
            }, function (errors) { _this.errors = _this.userService.handleError(errors); });
        }
    };
    LoginViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login-view',
            template: __webpack_require__(/*! ./login-view.component.html */ "./src/app/account/login-view/login-view.component.html"),
            styles: [__webpack_require__(/*! ./login-view.component.scss */ "./src/app/account/login-view/login-view.component.scss")]
        }),
        __metadata("design:paramtypes", [_share_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], LoginViewComponent);
    return LoginViewComponent;
}());



/***/ }),

/***/ "./src/app/account/register-view/register-view.component.html":
/*!********************************************************************!*\
  !*** ./src/app/account/register-view/register-view.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.0.8/css/all.css\">\r\n\r\n<div class=\"container\">\r\n  <br> \r\n\r\n  <div class=\"card bg-light\">\r\n    <article class=\"card-body mx-auto\" style=\"max-width: 400px;\">\r\n      <h4 class=\"card-title mt-3 text-center\">Create Account</h4>\r\n      <p class=\"text-center\">Get started with your free account</p>\r\n      <p>\r\n        <a href=\"\" class=\"btn btn-block btn-twitter\"> <i class=\"fab fa-twitter\"></i>   Login via Twitter</a>\r\n        <a href=\"\" class=\"btn btn-block btn-facebook\"> <i class=\"fab fa-facebook-f\"></i>   Login via facebook</a>\r\n      </p>\r\n      <p class=\"divider-text\">\r\n        <span class=\"bg-light\">OR</span>\r\n      </p>\r\n\r\n      <form #form=\"ngForm\" novalidate (ngSubmit)=\"registerUser(form)\">\r\n\r\n        <!-- firstName input// -->\r\n        <div class=\"form-group input-group\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\"> <i class=\"fa fa-user\"></i> </span>\r\n          </div>\r\n          <input id=\"first-name\" type=\"text\" required name=\"firstName\" class=\"form-control\" placeholder=\"Your first name\" tmFocus ngModel #firstName=\"ngModel\" minlength=\"3\">\r\n        </div>\r\n\r\n        <!-- firstName validate// -->\r\n        <div *ngIf=\"firstName.invalid && (firstName.dirty || firstName.touched)\">\r\n          <small *ngIf=\"firstName.errors.required\" style=\"color:red\">\r\n            First Name is required.\r\n          </small>\r\n          <small *ngIf=\"firstName.errors.minlength\" style=\"color:red\">\r\n            First Name is at least 3 character\r\n          </small>\r\n        </div><!-- form-group// -->\r\n        <!-- lastName input// -->\r\n        <div class=\"form-group input-group\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\"> <i class=\"fa fa-user\"></i> </span>\r\n          </div>\r\n          <input id=\"last-name\" type=\"text\" required name=\"lastName\" class=\"form-control\" placeholder=\"Your last name\" ngModel #lastName=\"ngModel\" minlength=\"3\">\r\n        </div>\r\n\r\n        <!-- lastName validate// -->\r\n        <div *ngIf=\"lastName.invalid && (lastName.dirty || lastName.touched)\">\r\n          <small *ngIf=\"lastName.errors.required\" style=\"color:red\">\r\n            Last Name is required.\r\n          </small>\r\n          <small *ngIf=\"lastName.errors.minlength\" style=\"color:red\">\r\n            Last Name is at least 3 character\r\n          </small>\r\n        </div><!-- form-group// -->\r\n        <!-- Location input// -->\r\n        <div class=\"form-group input-group\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\"> <i class=\"fa fa-building\"></i></span>\r\n          </div>\r\n          <input id=\"location\" type=\"text\" required name=\"location\" class=\"form-control\" placeholder=\"Location\" ngModel #location=\"ngModel\">\r\n        </div> <!-- form-group// -->\r\n\r\n\r\n        <!-- Email input// -->\r\n        <div class=\"form-group input-group\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\"> <i class=\"fa fa-envelope\"></i> </span>\r\n          </div>\r\n          <input id=\"email\" type=\"email\" required name=\"email\" validateEmail class=\"form-control\" placeholder=\"Email\" ngModel #email=\"ngModel\">\r\n        </div>\r\n        <!-- Email Validate// -->\r\n        <div>\r\n          <small [hidden]=\"email.valid || (email.pristine && !submitted)\" class=\"text-danger\">Please enter a valid email</small>\r\n        </div><!-- form-group// -->\r\n        <!-- Password input// -->\r\n        <div class=\"form-group input-group\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\"> <i class=\"fa fa-lock\"></i> </span>\r\n          </div>\r\n          <input id=\"password\" type=\"password\" required name=\"password\" class=\"form-control\" placeholder=\"Password\" ngModel #password=\"ngModel\" minlength=\"6\">\r\n        </div>\r\n\r\n        <!-- Password Validate// -->\r\n        <div *ngIf=\"password.invalid && (lastName.dirty || lastName.touched)\">\r\n          <small *ngIf=\"password.errors.minlength\" style=\"color:red\">\r\n            Password is at least 3 character\r\n          </small> <!-- form-group// -->\r\n        </div>\r\n\r\n        <!-- Submit Button// -->\r\n        <div class=\"form-group\">\r\n          <button type=\"submit\" class=\"btn  btn-primary btn-block\" [disabled]=\"form.invalid\">Sign Up</button>\r\n        </div> <!-- form-group// -->\r\n        <!-- Error console// -->\r\n        <div *ngIf=\"errors\" class=\"alert alert-danger\" role=\"alert\">\r\n          <strong>Oops!</strong> {{errors}}\r\n        </div>\r\n\r\n        <p class=\"text-center\">Have an account? <a routerLinkActive=\"active\" routerLink=\"/login\">Log In</a> </p>\r\n      </form>\r\n    </article>\r\n  </div> <!-- card.// -->\r\n</div>\r\n\r\n<!--container end.//-->\r\n"

/***/ }),

/***/ "./src/app/account/register-view/register-view.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/account/register-view/register-view.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".divider-text {\n  position: relative;\n  text-align: center;\n  margin-top: 15px;\n  margin-bottom: 15px; }\n\n.divider-text span {\n  padding: 7px;\n  font-size: 12px;\n  position: relative;\n  z-index: 2; }\n\n.divider-text:after {\n  content: \"\";\n  position: absolute;\n  width: 100%;\n  border-bottom: 1px solid #ddd;\n  top: 55%;\n  left: 0;\n  z-index: 1; }\n\n.btn-facebook {\n  background-color: #405D9D;\n  color: #fff; }\n\n.btn-twitter {\n  background-color: #42AEEC;\n  color: #fff; }\n"

/***/ }),

/***/ "./src/app/account/register-view/register-view.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/account/register-view/register-view.component.ts ***!
  \******************************************************************/
/*! exports provided: RegisterViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterViewComponent", function() { return RegisterViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _share_services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../share/services/user.service */ "./src/app/share/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RegisterViewComponent = /** @class */ (function () {
    function RegisterViewComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.submitted = false;
        this.role = "customer";
    }
    RegisterViewComponent.prototype.ngOnInit = function () {
    };
    RegisterViewComponent.prototype.registerUser = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        this.submitted = true;
        this.errors = '';
        if (valid) {
            this.userService.register(value.email, value.password, value.firstName, value.lastName, value.location, this.role)
                .subscribe(function (result) {
                if (result) {
                    _this.router.navigate(['/login'], { queryParams: { brandNew: true, email: value.email } });
                }
            }, function (errors) { _this.errors = _this.userService.handleError(errors); });
        }
    };
    RegisterViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register-view',
            template: __webpack_require__(/*! ./register-view.component.html */ "./src/app/account/register-view/register-view.component.html"),
            styles: [__webpack_require__(/*! ./register-view.component.scss */ "./src/app/account/register-view/register-view.component.scss")]
        }),
        __metadata("design:paramtypes", [_share_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], RegisterViewComponent);
    return RegisterViewComponent;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n\r\n<div class=\"container-fluid\">\r\n  <router-outlet></router-outlet>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _account_account_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./account/account.module */ "./src/app/account/account.module.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _share_services_config_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./share/services/config.service */ "./src/app/share/services/config.service.ts");
/* harmony import */ var _dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./dashboard/dashboard.module */ "./src/app/dashboard/dashboard.module.ts");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var src_app_authenticate_xhrbackend__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/authenticate-xhrbackend */ "./src/app/authenticate-xhrbackend.ts");
/* harmony import */ var _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @auth0/angular-jwt */ "./node_modules/@auth0/angular-jwt/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_3__["HeaderComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"]
            ],
            imports: [
                _account_account_module__WEBPACK_IMPORTED_MODULE_5__["AccountModule"],
                _dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_9__["DashboardModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_6__["HttpModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_10__["routing"]
            ],
            providers: [_share_services_config_service__WEBPACK_IMPORTED_MODULE_8__["ConfigService"], _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_12__["JwtHelperService"], {
                    provide: _angular_http__WEBPACK_IMPORTED_MODULE_6__["XHRBackend"],
                    useClass: src_app_authenticate_xhrbackend__WEBPACK_IMPORTED_MODULE_11__["AuthenticateXHRBackend"]
                }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: routing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routing", function() { return routing; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");


var appRoutes = [
    { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"] }
];
var routing = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(appRoutes);


/***/ }),

/***/ "./src/app/auth-guard.ts":
/*!*******************************!*\
  !*** ./src/app/auth-guard.ts ***!
  \*******************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _share_services_user_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./share/services/user.service */ "./src/app/share/services/user.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(user, router) {
        this.user = user;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        var token = localStorage.getItem('token');
        if (!this.user.isLoggedIn()) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_share_services_user_service__WEBPACK_IMPORTED_MODULE_0__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/authenticate-xhrbackend.ts":
/*!********************************************!*\
  !*** ./src/app/authenticate-xhrbackend.ts ***!
  \********************************************/
/*! exports provided: AuthenticateXHRBackend */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticateXHRBackend", function() { return AuthenticateXHRBackend; });
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthenticateXHRBackend = /** @class */ (function (_super) {
    __extends(AuthenticateXHRBackend, _super);
    function AuthenticateXHRBackend(_browserXhr, _baseResponseOptions, _xsrfStrategy) {
        return _super.call(this, _browserXhr, _baseResponseOptions, _xsrfStrategy) || this;
    }
    AuthenticateXHRBackend.prototype.createConnection = function (request) {
        var xhrConnection = _super.prototype.createConnection.call(this, request);
        xhrConnection.response = xhrConnection.response.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (error) {
            if ((error.status === 401 || error.status === 403) && (window.location.href.match(/\?/g) || []).length < 2) {
                console.log('The authentication session expired or the user is not authorized. Force refresh of the current page.');
                localStorage.removeItem('auth_token');
                window.location.href = window.location.href + '?' + new Date().getMilliseconds();
            }
            return error;
        }));
        return xhrConnection;
    };
    AuthenticateXHRBackend = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_0__["BrowserXhr"], _angular_http__WEBPACK_IMPORTED_MODULE_0__["ResponseOptions"], _angular_http__WEBPACK_IMPORTED_MODULE_0__["XSRFStrategy"]])
    ], AuthenticateXHRBackend);
    return AuthenticateXHRBackend;
}(_angular_http__WEBPACK_IMPORTED_MODULE_0__["XHRBackend"]));



/***/ }),

/***/ "./src/app/dashboard/dashboard.module.ts":
/*!***********************************************!*\
  !*** ./src/app/dashboard/dashboard.module.ts ***!
  \***********************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _dashboard_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard.routing */ "./src/app/dashboard/dashboard.routing.ts");
/* harmony import */ var _root_root_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./root/root.component */ "./src/app/dashboard/root/root.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "./src/app/dashboard/home/home.component.ts");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/dashboard/settings/settings.component.ts");
/* harmony import */ var _dashboard_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard.service */ "./src/app/dashboard/dashboard.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_share_share_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/share/share.module */ "./src/app/share/share.module.ts");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../auth-guard */ "./src/app/auth-guard.ts");
/* harmony import */ var src_app_share_services_pager_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/share/services/pager.service */ "./src/app/share/services/pager.service.ts");
/* harmony import */ var src_app_account_create_create_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/account/create/create.component */ "./src/app/account/create/create.component.ts");
/* harmony import */ var src_app_account_detail_detail_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/account/detail/detail.component */ "./src/app/account/detail/detail.component.ts");
/* harmony import */ var src_app_account_edit_edit_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/app/account/edit/edit.component */ "./src/app/account/edit/edit.component.ts");
/* harmony import */ var _management_management_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./management/management.component */ "./src/app/dashboard/management/management.component.ts");
/* harmony import */ var src_app_dashboard_management_user_list_user_list_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! src/app/dashboard/management/user-list/user-list.component */ "./src/app/dashboard/management/user-list/user-list.component.ts");
/* harmony import */ var src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! src/app/share/services/user.service */ "./src/app/share/services/user.service.ts");
/* harmony import */ var src_app_role_guard__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! src/app/role-guard */ "./src/app/role-guard.ts");
/* harmony import */ var _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @auth0/angular-jwt */ "./node_modules/@auth0/angular-jwt/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _dashboard_routing__WEBPACK_IMPORTED_MODULE_2__["routing"],
                src_app_share_share_module__WEBPACK_IMPORTED_MODULE_8__["ShareModule"]
            ],
            declarations: [_root_root_component__WEBPACK_IMPORTED_MODULE_3__["RootComponent"], _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"], _settings_settings_component__WEBPACK_IMPORTED_MODULE_5__["SettingsComponent"], _management_management_component__WEBPACK_IMPORTED_MODULE_14__["ManagementComponent"], src_app_account_create_create_component__WEBPACK_IMPORTED_MODULE_11__["CreateComponent"], src_app_account_edit_edit_component__WEBPACK_IMPORTED_MODULE_13__["EditComponent"], src_app_account_detail_detail_component__WEBPACK_IMPORTED_MODULE_12__["DetailComponent"], src_app_dashboard_management_user_list_user_list_component__WEBPACK_IMPORTED_MODULE_15__["UserListComponent"]],
            exports: [],
            providers: [_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"], _dashboard_service__WEBPACK_IMPORTED_MODULE_6__["DashboardService"], src_app_share_services_pager_service__WEBPACK_IMPORTED_MODULE_10__["PagerService"], src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_16__["UserService"], src_app_role_guard__WEBPACK_IMPORTED_MODULE_17__["RoleGuard"], _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_18__["JwtHelperService"]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.routing.ts":
/*!************************************************!*\
  !*** ./src/app/dashboard/dashboard.routing.ts ***!
  \************************************************/
/*! exports provided: routing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routing", function() { return routing; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home/home.component */ "./src/app/dashboard/home/home.component.ts");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/dashboard/settings/settings.component.ts");
/* harmony import */ var _root_root_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./root/root.component */ "./src/app/dashboard/root/root.component.ts");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth-guard */ "./src/app/auth-guard.ts");
/* harmony import */ var _management_management_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./management/management.component */ "./src/app/dashboard/management/management.component.ts");
/* harmony import */ var src_app_account_create_create_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/account/create/create.component */ "./src/app/account/create/create.component.ts");
/* harmony import */ var src_app_account_detail_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/account/detail/detail.component */ "./src/app/account/detail/detail.component.ts");
/* harmony import */ var src_app_account_edit_edit_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/account/edit/edit.component */ "./src/app/account/edit/edit.component.ts");
/* harmony import */ var src_app_dashboard_management_user_list_user_list_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/dashboard/management/user-list/user-list.component */ "./src/app/dashboard/management/user-list/user-list.component.ts");
/* harmony import */ var src_app_role_guard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/role-guard */ "./src/app/role-guard.ts");











var routing = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild([
    {
        path: 'dashboard',
        component: _root_root_component__WEBPACK_IMPORTED_MODULE_3__["RootComponent"], canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]],
        children: [
            { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"] },
            { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"] },
            {
                path: 'management', component: _management_management_component__WEBPACK_IMPORTED_MODULE_5__["ManagementComponent"], canActivate: [src_app_role_guard__WEBPACK_IMPORTED_MODULE_10__["RoleGuard"]], data: { expectedRole: 'Admin' }, children: [
                    { path: '', component: src_app_dashboard_management_user_list_user_list_component__WEBPACK_IMPORTED_MODULE_9__["UserListComponent"] },
                    { path: 'create', component: src_app_account_create_create_component__WEBPACK_IMPORTED_MODULE_6__["CreateComponent"] },
                    { path: 'detail', component: src_app_account_detail_detail_component__WEBPACK_IMPORTED_MODULE_7__["DetailComponent"] },
                    { path: 'edit', component: src_app_account_edit_edit_component__WEBPACK_IMPORTED_MODULE_8__["EditComponent"] }
                ]
            },
            { path: 'settings', component: _settings_settings_component__WEBPACK_IMPORTED_MODULE_2__["SettingsComponent"] },
        ]
    }
]);


/***/ }),

/***/ "./src/app/dashboard/dashboard.service.ts":
/*!************************************************!*\
  !*** ./src/app/dashboard/dashboard.service.ts ***!
  \************************************************/
/*! exports provided: DashboardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardService", function() { return DashboardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _share_services_base_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../share/services/base.service */ "./src/app/share/services/base.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _share_services_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../share/services/config.service */ "./src/app/share/services/config.service.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DashboardService = /** @class */ (function (_super) {
    __extends(DashboardService, _super);
    function DashboardService(http, configService) {
        var _this = _super.call(this) || this;
        _this.http = http;
        _this.configService = configService;
        _this.baseUrl = '';
        _this.baseUrl = configService.getApiURI();
        return _this;
    }
    DashboardService.prototype.getHomeDetails = function () {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var authToken = localStorage.getItem('auth_token');
        headers.append('Authorization', "Bearer " + authToken);
        return this.http.get(this.baseUrl + "/dashboard/home", { headers: headers });
    };
    DashboardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _share_services_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]])
    ], DashboardService);
    return DashboardService;
}(_share_services_base_service__WEBPACK_IMPORTED_MODULE_1__["BaseService"]));



/***/ }),

/***/ "./src/app/dashboard/home/home.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/home/home.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css\" rel=\"stylesheet\">\r\n<div id=\"user-profile-2\" class=\"user-profile\">\r\n  <div class=\"tabbable\">\r\n    <ul class=\"nav nav-tabs padding-18\">\r\n      <li class=\"active\">\r\n        <a data-toggle=\"tab\" href=\"#home\">\r\n          <i class=\"green ace-icon fa fa-user bigger-120\"></i>\r\n          Profile\r\n        </a>\r\n      </li>\r\n\r\n      <li>\r\n        <a data-toggle=\"tab\" href=\"#feed\">\r\n          <i class=\"orange ace-icon fa fa-rss bigger-120\"></i>\r\n          Activity Feed\r\n        </a>\r\n      </li>\r\n\r\n      <li>\r\n        <a data-toggle=\"tab\" href=\"#friends\">\r\n          <i class=\"blue ace-icon fa fa-users bigger-120\"></i>\r\n          Friends\r\n        </a>\r\n      </li>\r\n\r\n      <li>\r\n        <a data-toggle=\"tab\" href=\"#pictures\">\r\n          <i class=\"pink ace-icon fa fa-picture-o bigger-120\"></i>\r\n          Pictures\r\n        </a>\r\n      </li>\r\n    </ul>\r\n\r\n    <div class=\"tab-content no-border padding-24\">\r\n      <div id=\"home\" class=\"tab-pane in active\">\r\n        <div class=\"row\">\r\n          <div class=\"col-xs-12 col-sm-3 center\">\r\n            <span class=\"profile-picture\">\r\n              <img class=\"editable img-responsive\" alt=\" Avatar\" id=\"avatar2\" src=\"https://bootdey.com/img/Content/avatar/avatar6.png\">\r\n            </span>\r\n\r\n            <div class=\"space space-4\"></div>\r\n\r\n            <a href=\"#\" class=\"btn btn-sm btn-block btn-success\">\r\n              <i class=\"ace-icon fa fa-plus-circle bigger-120 .btn-space\"></i>\r\n              <span class=\"bigger-110 .btn-space\">Edit Profile</span>\r\n            </a>\r\n\r\n            <a href=\"#\" class=\"btn btn-sm btn-block btn-primary\">\r\n              <i class=\"ace-icon fa fa-envelope-o bigger-110 .btn-space\"></i>\r\n              <span class=\"bigger-110 .btn-space\">Do Something</span>\r\n            </a>\r\n          </div><!-- /.col -->\r\n\r\n          <div class=\"col-xs-12 col-sm-9\">\r\n            <h4 class=\"blue\">\r\n              <span class=\"middle\">{{userDetails.firstName}} {{userDetails.lastName}}</span>\r\n\r\n              <!--<span class=\"label label-purple arrowed-in-right\">\r\n                <i class=\"ace-icon fa fa-circle smaller-80 align-middle\"></i>\r\n               \r\n              </span>-->\r\n            </h4>\r\n\r\n            <div class=\"profile-user-info\">\r\n              <div class=\"profile-info-row\">\r\n                <div class=\"profile-info-name\"> Email </div>\r\n\r\n                <div class=\"profile-info-value\">\r\n                  <span>{{userDetails.userName}}</span>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"profile-info-row\">\r\n                <div class=\"profile-info-name\"> Location </div>\r\n\r\n                <div class=\"profile-info-value\">\r\n                  <i class=\"fa fa-map-marker light-orange bigger-110\"></i>\r\n                  <span>{{userDetails.location}}</span>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"profile-info-row\">\r\n                <div class=\"profile-info-name\"> Age </div>\r\n\r\n                <div class=\"profile-info-value\">\r\n                  <span>38</span>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"profile-info-row\">\r\n                <div class=\"profile-info-name\"> Joined </div>\r\n\r\n                <div class=\"profile-info-value\">\r\n                  <span>2010/06/20</span>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"profile-info-row\">\r\n                <div class=\"profile-info-name\"> Last Online </div>\r\n\r\n                <div class=\"profile-info-value\">\r\n                  <span>3 hours ago</span>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"hr hr-8 dotted\"></div>\r\n\r\n            <div class=\"profile-user-info\">\r\n              <div class=\"profile-info-row\">\r\n                <div class=\"profile-info-name\"> Website </div>\r\n\r\n                <div class=\"profile-info-value\">\r\n                  <a href=\"#\" target=\"_blank\">www.TEST.com</a>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"profile-info-row\">\r\n                <div class=\"profile-info-name\">\r\n                  <i class=\"middle ace-icon fa fa-facebook-square bigger-150 blue\"></i>\r\n                </div>\r\n\r\n                <div class=\"profile-info-value\">\r\n                  <a href=\"#\">Find me on Facebook</a>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"profile-info-row\">\r\n                <div class=\"profile-info-name\">\r\n                  <i class=\"middle ace-icon fa fa-twitter-square bigger-150 light-blue\"></i>\r\n                </div>\r\n\r\n                <div class=\"profile-info-value\">\r\n                  <a href=\"#\">Follow me on Twitter</a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div><!-- /.col -->\r\n        </div><!-- /.row -->\r\n\r\n        <div class=\"space-20\"></div>\r\n\r\n        <div class=\"row\">\r\n          <div class=\"col-xs-12 col-sm-6\">\r\n            <div class=\"widget-box transparent\">\r\n              <div class=\"widget-header widget-header-small\">\r\n                <h4 class=\"widget-title smaller\">\r\n                  <i class=\"ace-icon fa fa-check-square-o bigger-110\"></i>\r\n                  Little About Me\r\n                </h4>\r\n              </div>\r\n\r\n              <div class=\"widget-body\">\r\n                <div class=\"widget-main\">\r\n                  <p>\r\n                     Life is such chaos, and I am so tired.\r\n                  </p>\r\n                  \r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div><!-- /#home -->\r\n</div>\r\n    </div></div>\r\n"

/***/ }),

/***/ "./src/app/dashboard/home/home.component.scss":
/*!****************************************************!*\
  !*** ./src/app/dashboard/home/home.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*.acc-col {\n  min-height: 300px;\n  margin-top: 5%;\n  background-color: #ffffff;\n  padding: 20px;\n  \n}\n\n.acc-col > h3 {\n  padding-bottom: 20px;\n}\n\nhr {\n  border: solid 1px #d6d6d6;\n}\n\nsection > form > div {\n  padding: 5px;\n}\n\n.btn-div {\n  margin-top: 10px;\n  padding-bottom: 10px;\n}\n\n\n.ui-67 .ui-head {\n  text-align: center;\n  padding: 30px 0px;\n  position: relative;\n  border-bottom: 2px solid #fff;\n  box-shadow: 0px 0px 5px rgba(0,0,0,0.1);\n  background-color: #1997c6;\n}\n\n.ui-67 .ui-head .ui-details {\n  margin: 0px 0px 74px;\n}\n\n.ui-67 .ui-head .ui-details h3 {\n  color: #fff;\n  font-size: 40px;\n  line-height: 60px;\n  font-weight: 300;\n}\n\n@media (max-width:400px) {\n  .ui-67 .ui-head .ui-details h3 {\n    font-size: 25px;\n    line-height: 40px;\n    font-weight: 400;\n  }\n}\n\n.ui-67 .ui-head .ui-details h4 {\n  color: #fff;\n  font-size: 18px;\n  line-height: 38px;\n  font-weight: 400;\n}\n\n.ui-67 .ui-head .ui-image {\n  width: 100%;\n  position: absolute;\n  bottom: -55px;\n  z-index: 10;\n}\n\n.ui-67 .ui-head img {\n  width: 120px;\n  border-radius: 100%;\n  margin: 0px auto;\n  border: 4px solid #fff;\n  box-shadow: 0px 0px 15px rgba(0,0,0,0.1);\n}*/\nbody {\n  margin-top: 20px; }\n.align-center, .center {\n  text-align: center !important; }\n.profile-user-info {\n  display: table;\n  width: 98%;\n  width: calc(100% - 24px);\n  margin: 0 auto; }\n.profile-info-row {\n  display: table-row; }\n.profile-info-name,\n.profile-info-value {\n  display: table-cell;\n  border-top: 1px dotted #D5E4F1; }\n.profile-info-name {\n  text-align: right;\n  padding: 6px 10px 6px 4px;\n  font-weight: 400;\n  color: #667E99;\n  background-color: transparent;\n  width: 110px;\n  vertical-align: middle; }\n.profile-info-value {\n  padding: 6px 4px 6px 6px; }\n.profile-info-value > span + span:before {\n  display: inline;\n  content: \",\";\n  margin-left: 1px;\n  margin-right: 3px;\n  color: #666;\n  border-bottom: 1px solid #FFF; }\n.profile-info-value > span + span.editable-container:before {\n  display: none; }\n.profile-info-row:first-child .profile-info-name,\n.profile-info-row:first-child .profile-info-value {\n  border-top: none; }\n.profile-user-info-striped {\n  border: 1px solid #DCEBF7; }\n.profile-user-info-striped .profile-info-name {\n  color: #336199;\n  background-color: #EDF3F4;\n  border-top: 1px solid #F7FBFF; }\n.profile-user-info-striped .profile-info-value {\n  border-top: 1px dotted #DCEBF7;\n  padding-left: 12px; }\n.profile-picture {\n  border: 1px solid #CCC;\n  background-color: #FFF;\n  padding: 4px;\n  display: inline-block;\n  max-width: 100%;\n  box-sizing: border-box;\n  box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.15); }\n.dd-empty,\n.dd-handle,\n.dd-placeholder,\n.dd2-content {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box; }\n.profile-activity {\n  padding: 10px 4px;\n  border-bottom: 1px dotted #D0D8E0;\n  position: relative;\n  border-left: 1px dotted #FFF;\n  border-right: 1px dotted #FFF; }\n.profile-activity:first-child {\n  border-top: 1px dotted transparent; }\n.profile-activity:first-child:hover {\n  border-top-color: #D0D8E0; }\n.profile-activity:hover {\n  background-color: #F4F9FD;\n  border-left: 1px dotted #D0D8E0;\n  border-right: 1px dotted #D0D8E0; }\n.profile-activity img {\n  border: 2px solid #C9D6E5;\n  border-radius: 100%;\n  max-width: 40px;\n  margin-right: 10px;\n  margin-left: 0;\n  box-shadow: none; }\n.profile-activity .thumbicon {\n  background-color: #74ABD7;\n  display: inline-block;\n  border-radius: 100%;\n  width: 38px;\n  height: 38px;\n  color: #FFF;\n  font-size: 18px;\n  text-align: center;\n  line-height: 38px;\n  margin-right: 10px;\n  margin-left: 0;\n  text-shadow: none !important; }\n.profile-activity .time {\n  display: block;\n  margin-top: 4px;\n  color: #777; }\n.profile-activity a.user {\n  font-weight: 700;\n  color: #9585BF; }\n.profile-activity .tools {\n  position: absolute;\n  right: 12px;\n  bottom: 8px;\n  display: none; }\n.profile-activity:hover .tools {\n  display: block; }\n.user-profile .ace-thumbnails li {\n  border: 1px solid #CCC;\n  padding: 3px;\n  margin: 6px; }\n.user-profile .ace-thumbnails li .tools {\n  left: 3px;\n  right: 3px; }\n.user-profile .ace-thumbnails li:hover .tools {\n  bottom: 3px; }\n.user-title-label:hover {\n  text-decoration: none; }\n.user-title-label + .dropdown-menu {\n  margin-left: -12px; }\n.profile-contact-links {\n  padding: 4px 2px 5px;\n  border: 1px solid #E0E2E5;\n  background-color: #F8FAFC; }\n.btn-link:hover .ace-icon {\n  text-decoration: none !important; }\n.profile-social-links > a:hover > .ace-icon,\n.profile-users .memberdiv .name a:hover .ace-icon,\n.profile-users .memberdiv .tools > a:hover {\n  text-decoration: none; }\n.profile-social-links > a {\n  text-decoration: none;\n  margin: 0 1px; }\n.profile-skills .progress {\n  height: 26px;\n  margin-bottom: 2px;\n  background-color: transparent; }\n.profile-skills .progress .progress-bar {\n  line-height: 26px;\n  font-size: 13px;\n  font-weight: 700;\n  font-family: \"Open Sans\";\n  padding: 0 8px; }\n.profile-users .user {\n  display: block;\n  position: static;\n  text-align: center;\n  width: auto; }\n.profile-users .user img {\n  padding: 2px;\n  border-radius: 100%;\n  border: 1px solid #AAA;\n  max-width: none;\n  width: 64px;\n  transition: all .1s; }\n.profile-users .user img:hover {\n  box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.33); }\n.profile-users .memberdiv {\n  background-color: #FFF;\n  width: 100px;\n  box-sizing: border-box;\n  border: none;\n  text-align: center;\n  margin: 0 8px 24px; }\n.profile-users .memberdiv .body {\n  display: inline-block;\n  margin: 8px 0 0; }\n.profile-users .memberdiv .popover {\n  visibility: hidden;\n  min-width: 0;\n  max-height: 0;\n  max-width: 0;\n  margin-left: 0;\n  margin-right: 0;\n  top: -5%;\n  left: auto;\n  right: auto;\n  opacity: 0;\n  display: none;\n  position: absolute;\n  transition: opacity .2s linear 0s, visibility 0s linear .2s, max-height 0s linear .2s, max-width 0s linear .2s, min-width 0s linear .2s; }\n.profile-users .memberdiv .popover.right {\n  left: 100%;\n  right: auto;\n  display: block; }\n.profile-users .memberdiv .popover.left {\n  left: auto;\n  right: 100%;\n  display: block; }\n.profile-users .memberdiv > :first-child:hover .popover {\n  visibility: visible;\n  opacity: 1;\n  z-index: 1060;\n  max-height: 250px;\n  max-width: 250px;\n  min-width: 150px;\n  transition-delay: 0s; }\n.profile-users .memberdiv .tools {\n  position: static;\n  display: block;\n  width: 100%;\n  margin-top: 2px; }\n.profile-users .memberdiv .tools > a {\n  margin: 0 2px; }\n.user-status {\n  display: inline-block;\n  width: 11px;\n  height: 11px;\n  background-color: #FFF;\n  border: 3px solid #AAA;\n  border-radius: 100%;\n  vertical-align: middle;\n  margin-right: 1px; }\n.user-status.status-online {\n  border-color: #8AC16C; }\n.user-status.status-busy {\n  border-color: #E07F69; }\n.user-status.status-idle {\n  border-color: #FFB752; }\n.tab-content.profile-edit-tab-content {\n  border: 1px solid #DDD;\n  padding: 8px 32px 32px;\n  box-shadow: 1px 1px 0 0 rgba(0, 0, 0, 0.2);\n  background-color: #FFF; }\n@media only screen and (max-width: 480px) {\n  .profile-info-name {\n    width: 80px; }\n  .profile-user-info-striped .profile-info-name {\n    float: none;\n    width: auto;\n    text-align: left;\n    padding: 6px 4px 6px 10px;\n    display: block; }\n  .profile-user-info-striped .profile-info-value {\n    margin-left: 10px;\n    display: block; }\n  .user-profile .memberdiv {\n    width: 50%;\n    margin-left: 0;\n    margin-right: 0; } }\n.itemdiv {\n  padding-right: 3px;\n  min-height: 66px; }\n.itemdiv > .user {\n  display: inline-block;\n  width: 42px;\n  position: absolute;\n  left: 0; }\n.itemdiv > .user > .img,\n.itemdiv > .user > img {\n  border-radius: 100%;\n  border: 2px solid #5293C4;\n  max-width: 40px;\n  position: relative; }\n.itemdiv > .user > .img {\n  padding: 2px; }\n.itemdiv > .body {\n  width: auto;\n  margin-left: 50px;\n  margin-right: 12px;\n  position: relative; }\n.itemdiv > .body > .time {\n  display: block;\n  font-size: 11px;\n  font-weight: 700;\n  color: #666;\n  position: absolute;\n  right: 9px;\n  top: 0; }\n.itemdiv > .body > .time .ace-icon {\n  font-size: 14px;\n  font-weight: 400; }\n.itemdiv > .body > .name {\n  display: block;\n  color: #999; }\n.itemdiv > .body > .name > b {\n  color: #777; }\n.itemdiv > .body > .text {\n  display: block;\n  position: relative;\n  margin-top: 2px;\n  padding-bottom: 19px;\n  padding-left: 7px;\n  font-size: 13px; }\n.itemdiv.dialogdiv:before,\n.itemdiv.dialogdiv > .body:before,\n.itemdiv > .body > .text:after {\n  content: \"\";\n  position: absolute; }\n.itemdiv > .body > .text:after {\n  display: block;\n  height: 1px;\n  font-size: 0;\n  overflow: hidden;\n  left: 16px;\n  right: -12px;\n  margin-top: 9px;\n  border-top: 1px solid #E4ECF3; }\n.itemdiv > .body > .text > .ace-icon:first-child {\n  color: #DCE3ED;\n  margin-right: 4px; }\n.itemdiv:last-child > .body > .text {\n  border-bottom-width: 0; }\n.itemdiv:last-child > .body > .text:after {\n  display: none; }\n.itemdiv.dialogdiv {\n  padding-bottom: 14px; }\n.itemdiv.dialogdiv:before {\n  display: block;\n  top: 0;\n  bottom: 0;\n  left: 19px;\n  width: 3px;\n  max-width: 3px;\n  background-color: #E1E6ED;\n  border: 1px solid #D7DBDD;\n  border-width: 0 1px; }\n.itemdiv.dialogdiv:last-child {\n  padding-bottom: 0; }\n.itemdiv.dialogdiv:last-child:before {\n  display: none; }\n.itemdiv.dialogdiv > .user > img {\n  border-color: #C9D6E5; }\n.itemdiv.dialogdiv > .body {\n  border: 1px solid #DDE4ED;\n  padding: 5px 8px 8px;\n  border-left-width: 2px;\n  margin-right: 1px; }\n.itemdiv.dialogdiv > .body:before {\n  display: block;\n  left: -7px;\n  top: 11px;\n  width: 8px;\n  height: 8px;\n  border: 2px solid #DDE4ED;\n  border-width: 2px 0 0 2px;\n  background-color: #FFF;\n  box-sizing: content-box;\n  -webkit-transform: rotate(-45deg);\n  transform: rotate(-45deg); }\n.itemdiv.dialogdiv > .body > .time {\n  position: static;\n  float: right; }\n.itemdiv.dialogdiv > .body > .text {\n  padding-left: 0;\n  padding-bottom: 0; }\n.itemdiv.dialogdiv > .body > .text:after {\n  display: none; }\n.itemdiv.dialogdiv .tooltip-inner {\n  word-break: break-all; }\n.itemdiv.memberdiv {\n  width: 175px;\n  padding: 2px;\n  margin: 3px 0;\n  float: left;\n  border-bottom: 1px solid #E8E8E8; }\n@media (min-width: 992px) {\n  .itemdiv.memberdiv {\n    max-width: 50%; } }\n@media (max-width: 991px) {\n  .itemdiv.memberdiv {\n    min-width: 33.333%; } }\n.itemdiv.memberdiv > .user > img {\n  border-color: #DCE3ED; }\n.itemdiv.memberdiv > .body > .time {\n  position: static; }\n.itemdiv.memberdiv > .body > .name {\n  line-height: 18px;\n  height: 18px;\n  margin-bottom: 0; }\n.itemdiv.memberdiv > .body > .name > a {\n  display: inline-block;\n  max-width: 100px;\n  max-height: 18px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  word-break: break-all; }\n.itemdiv .tools {\n  position: absolute;\n  right: 5px;\n  bottom: 10px;\n  display: none; }\n.item-list > li > .checkbox,\n.item-list > li > label.inline,\n.itemdiv:hover .tools {\n  display: inline-block; }\n.itemdiv .tools .btn {\n  border-radius: 36px;\n  margin: 1px 0; }\n.itemdiv .body .tools {\n  bottom: 4px; }\n.itemdiv.commentdiv .tools {\n  right: 9px; }\n.item-list {\n  margin: 0;\n  padding: 0;\n  list-style: none; }\n.item-list > li {\n  padding: 9px;\n  background-color: #FFF;\n  margin-top: -1px;\n  position: relative; }\n.item-list > li.selected {\n  color: #8090A0;\n  background-color: #F4F9FC; }\n.item-list > li.selected .lbl,\n.item-list > li.selected label {\n  text-decoration: line-through;\n  color: #8090A0; }\n.item-list > li label {\n  font-size: 13px; }\n.item-list > li .percentage {\n  font-size: 11px;\n  font-weight: 700;\n  color: #777; }\n.ace-thumbnails > li,\n.ace-thumbnails > li > :first-child {\n  display: block;\n  position: relative; }\n.ace-thumbnails {\n  list-style: none;\n  margin: 0;\n  padding: 0; }\n.ace-thumbnails > li {\n  float: left;\n  overflow: hidden;\n  margin: 2px;\n  border: 2px solid #333; }\n.ace-thumbnails > li > :first-child:focus {\n  outline: 0; }\n.ace-thumbnails > li .tags {\n  display: inline-block;\n  position: absolute;\n  bottom: 0;\n  right: 0;\n  overflow: visible;\n  direction: rtl;\n  padding: 0;\n  margin: 0;\n  height: auto;\n  width: auto;\n  background-color: transparent;\n  border-width: 0;\n  vertical-align: inherit; }\n.ace-thumbnails > li .tags > .label-holder {\n  opacity: .92;\n  filter: alpha(opacity=92);\n  display: table;\n  margin: 1px 0 0;\n  direction: ltr;\n  text-align: left; }\n.ace-thumbnails > li > .tools,\n.ace-thumbnails > li > :first-child > .text {\n  position: absolute;\n  text-align: center;\n  background-color: rgba(0, 0, 0, 0.55); }\n.ace-thumbnails > li .tags > .label-holder:hover {\n  opacity: 1;\n  filter: alpha(opacity=100); }\n.ace-thumbnails > li > .tools {\n  top: 0;\n  bottom: 0;\n  left: -30px;\n  width: 24px;\n  vertical-align: middle;\n  transition: all .2s ease; }\n.ace-thumbnails > li > .tools.tools-right {\n  left: auto;\n  right: -30px; }\n.ace-thumbnails > li > .tools.tools-bottom {\n  width: auto;\n  height: 28px;\n  left: 0;\n  right: 0;\n  top: auto;\n  bottom: -30px; }\n.ace-thumbnails > li > .tools.tools-top {\n  width: auto;\n  height: 28px;\n  left: 0;\n  right: 0;\n  top: -30px;\n  bottom: auto; }\n.ace-thumbnails > li:hover > .tools {\n  left: 0;\n  right: 0; }\n.ace-thumbnails > li:hover > .tools.tools-bottom {\n  top: auto;\n  bottom: 0; }\n.ace-thumbnails > li:hover > .tools.tools-top {\n  bottom: auto;\n  top: 0; }\n.ace-thumbnails > li:hover > .tools.tools-right {\n  left: auto;\n  right: 0; }\n.ace-thumbnails > li > .in.tools {\n  left: 0;\n  right: 0; }\n.ace-thumbnails > li > .in.tools.tools-bottom {\n  top: auto;\n  bottom: 0; }\n.ace-thumbnails > li > .in.tools.tools-top {\n  bottom: auto;\n  top: 0; }\n.ace-thumbnails > li > .in.tools.tools-right {\n  left: auto;\n  right: 0; }\n.ace-thumbnails > li > .tools > a,\n.ace-thumbnails > li > :first-child .inner a {\n  display: inline-block;\n  color: #FFF;\n  font-size: 18px;\n  font-weight: 400;\n  padding: 0 4px; }\n.ace-thumbnails > li > .tools > a:hover,\n.ace-thumbnails > li > :first-child .inner a:hover {\n  text-decoration: none;\n  color: #C9E2EA; }\n.ace-thumbnails > li .tools.tools-bottom > a,\n.ace-thumbnails > li .tools.tools-top > a {\n  display: inline-block; }\n.ace-thumbnails > li > :first-child > .text {\n  right: 0;\n  left: 0;\n  bottom: 0;\n  top: 0;\n  color: #FFF;\n  opacity: 0;\n  filter: alpha(opacity=0);\n  transition: all .2s ease; }\n.dialogs,\n.itemdiv {\n  position: relative; }\n.ace-thumbnails > li > :first-child > .text:before {\n  content: '';\n  display: inline-block;\n  height: 100%;\n  vertical-align: middle;\n  margin-right: 0; }\n.ace-thumbnails > li > :first-child > .text > .inner {\n  padding: 4px 0;\n  margin: 0;\n  display: inline-block;\n  vertical-align: middle;\n  max-width: 90%; }\n.ace-thumbnails > li:hover > :first-child > .text {\n  opacity: 1;\n  filter: alpha(opacity=100); }\n@media only screen and (max-width: 480px) {\n  .ace-thumbnails {\n    text-align: center; }\n  .ace-thumbnails > li {\n    float: none;\n    display: inline-block; } }\n.tab-content {\n  border: 1px solid #C5D0DC;\n  padding: 16px 12px;\n  position: relative; }\n.tab-content.no-padding {\n  padding: 0; }\n.tab-content.no-border {\n  border: none;\n  padding: 12px; }\n.tab-content.padding-32 {\n  padding: 32px 24px; }\n.tab-content.no-border.padding-32 {\n  padding: 32px; }\n.tab-content.padding-30 {\n  padding: 30px 23px; }\n.tab-content.no-border.padding-30 {\n  padding: 30px; }\n.tab-content.padding-28 {\n  padding: 28px 21px; }\n.tab-content.no-border.padding-28 {\n  padding: 28px; }\n.tab-content.padding-26 {\n  padding: 26px 20px; }\n.tab-content.no-border.padding-26 {\n  padding: 26px; }\n.tab-content.padding-24 {\n  padding: 24px 18px; }\n.tab-content.no-border.padding-24 {\n  padding: 24px; }\n.tab-content.padding-22 {\n  padding: 22px 17px; }\n.tab-content.no-border.padding-22 {\n  padding: 22px; }\n.tab-content.padding-20 {\n  padding: 20px 15px; }\n.tab-content.no-border.padding-20 {\n  padding: 20px; }\n.tab-content.padding-18 {\n  padding: 18px 14px; }\n.tab-content.no-border.padding-18 {\n  padding: 18px; }\n.tab-content.padding-16 {\n  padding: 16px 12px; }\n.tab-content.no-border.padding-16 {\n  padding: 16px; }\n.tab-content.padding-14 {\n  padding: 14px 11px; }\n.tab-content.no-border.padding-14 {\n  padding: 14px; }\n.tab-content.padding-12 {\n  padding: 12px 9px; }\n.tab-content.no-border.padding-12 {\n  padding: 12px; }\n.tab-content.padding-10 {\n  padding: 10px 8px; }\n.tab-content.no-border.padding-10 {\n  padding: 10px; }\n.tab-content.padding-8 {\n  padding: 8px 6px; }\n.tab-content.no-border.padding-8 {\n  padding: 8px; }\n.tab-content.padding-6 {\n  padding: 6px 5px; }\n.tab-content.no-border.padding-6 {\n  padding: 6px; }\n.tab-content.padding-4 {\n  padding: 4px 3px; }\n.tab-content.no-border.padding-4 {\n  padding: 4px; }\n.tab-content.no-border.padding-2,\n.tab-content.padding-2 {\n  padding: 2px; }\n.tab-content.no-border.padding-0,\n.tab-content.padding-0 {\n  padding: 0; }\n.nav.nav-tabs.padding-28 {\n  padding-left: 28px; }\n.tabs-left > .nav.nav-tabs.padding-28,\n.tabs-right > .nav.nav-tabs.padding-28 {\n  padding-left: 0;\n  padding-top: 28px; }\n.nav.nav-tabs.padding-26 {\n  padding-left: 26px; }\n.tabs-left > .nav.nav-tabs.padding-26,\n.tabs-right > .nav.nav-tabs.padding-26 {\n  padding-left: 0;\n  padding-top: 26px; }\n.nav.nav-tabs.padding-24 {\n  padding-left: 24px; }\n.tabs-left > .nav.nav-tabs.padding-24,\n.tabs-right > .nav.nav-tabs.padding-24 {\n  padding-left: 0;\n  padding-top: 24px; }\n.nav.nav-tabs.padding-22 {\n  padding-left: 22px; }\n.tabs-left > .nav.nav-tabs.padding-22,\n.tabs-right > .nav.nav-tabs.padding-22 {\n  padding-left: 0;\n  padding-top: 22px; }\n.nav.nav-tabs.padding-20 {\n  padding-left: 20px; }\n.tabs-left > .nav.nav-tabs.padding-20,\n.tabs-right > .nav.nav-tabs.padding-20 {\n  padding-left: 0;\n  padding-top: 20px; }\n.nav.nav-tabs.padding-18 {\n  padding-left: 18px; }\n.tabs-left > .nav.nav-tabs.padding-18,\n.tabs-right > .nav.nav-tabs.padding-18 {\n  padding-left: 0;\n  padding-top: 18px; }\n.nav.nav-tabs.padding-16 {\n  padding-left: 16px; }\n.tabs-left > .nav.nav-tabs.padding-16,\n.tabs-right > .nav.nav-tabs.padding-16 {\n  padding-left: 0;\n  padding-top: 16px; }\n.nav.nav-tabs.padding-14 {\n  padding-left: 14px; }\n.tabs-left > .nav.nav-tabs.padding-14,\n.tabs-right > .nav.nav-tabs.padding-14 {\n  padding-left: 0;\n  padding-top: 14px; }\n.nav.nav-tabs.padding-12 {\n  padding-left: 12px; }\n.tabs-left > .nav.nav-tabs.padding-12,\n.tabs-right > .nav.nav-tabs.padding-12 {\n  padding-left: 0;\n  padding-top: 12px; }\n.nav.nav-tabs.padding-10 {\n  padding-left: 10px; }\n.tabs-left > .nav.nav-tabs.padding-10,\n.tabs-right > .nav.nav-tabs.padding-10 {\n  padding-left: 0;\n  padding-top: 10px; }\n.nav.nav-tabs.padding-8 {\n  padding-left: 8px; }\n.tabs-left > .nav.nav-tabs.padding-8,\n.tabs-right > .nav.nav-tabs.padding-8 {\n  padding-left: 0;\n  padding-top: 8px; }\n.nav.nav-tabs.padding-6 {\n  padding-left: 6px; }\n.tabs-left > .nav.nav-tabs.padding-6,\n.tabs-right > .nav.nav-tabs.padding-6 {\n  padding-left: 0;\n  padding-top: 6px; }\n.nav.nav-tabs.padding-4 {\n  padding-left: 4px; }\n.tabs-left > .nav.nav-tabs.padding-4,\n.tabs-right > .nav.nav-tabs.padding-4 {\n  padding-left: 0;\n  padding-top: 4px; }\n.nav.nav-tabs.padding-2 {\n  padding-left: 2px; }\n.tabs-left > .nav.nav-tabs.padding-2,\n.tabs-right > .nav.nav-tabs.padding-2 {\n  padding-left: 0;\n  padding-top: 2px; }\n.nav-tabs {\n  border-color: #C5D0DC;\n  margin-bottom: 0 !important;\n  position: relative;\n  top: 1px; }\n.nav-tabs > li > a {\n  padding: 7px 12px 8px; }\n.nav-tabs > li > a,\n.nav-tabs > li > a:focus {\n  border-radius: 0 !important;\n  border-color: #C5D0DC;\n  background-color: #F9F9F9;\n  color: #999;\n  margin-right: -1px;\n  line-height: 18px;\n  position: relative; }\n.nav-tabs > li > a:hover {\n  background-color: #FFF;\n  color: #4C8FBD;\n  border-color: #C5D0DC; }\n.nav-tabs > li > a:active,\n.nav-tabs > li > a:focus {\n  outline: 0 !important; }\n.nav-tabs > li.active > a,\n.nav-tabs > li.active > a:focus,\n.nav-tabs > li.active > a:hover {\n  color: #576373;\n  border-color: #C5D0DC #C5D0DC transparent;\n  border-top: 2px solid #4C8FBD;\n  background-color: #FFF;\n  z-index: 1;\n  line-height: 18px;\n  margin-top: -1px;\n  box-shadow: 0 -2px 3px 0 rgba(0, 0, 0, 0.15); }\n.tabs-below > .nav-tabs {\n  top: auto;\n  margin-bottom: 0;\n  margin-top: -1px;\n  border-color: #C5D0DC;\n  border-bottom-width: 0; }\n.tabs-below > .nav-tabs > li > a,\n.tabs-below > .nav-tabs > li > a:focus,\n.tabs-below > .nav-tabs > li > a:hover {\n  border-color: #C5D0DC; }\n.tabs-below > .nav-tabs > li.active > a,\n.tabs-below > .nav-tabs > li.active > a:focus,\n.tabs-below > .nav-tabs > li.active > a:hover {\n  border-color: transparent #C5D0DC #C5D0DC;\n  border-top-width: 1px;\n  border-bottom: 2px solid #4C8FBD;\n  margin-top: 0;\n  box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.15); }\n.tabs-left > .nav-tabs > li > a,\n.tabs-right > .nav-tabs > li > a {\n  min-width: 60px; }\n.tabs-left > .nav-tabs {\n  top: auto;\n  margin-bottom: 0;\n  border-color: #C5D0DC;\n  float: left; }\n.tabs-left > .nav-tabs > li {\n  float: none !important; }\n.tabs-left > .nav-tabs > li > a,\n.tabs-left > .nav-tabs > li > a:focus,\n.tabs-left > .nav-tabs > li > a:hover {\n  border-color: #C5D0DC;\n  margin: 0 -1px 0 0; }\n.tabs-left > .nav-tabs > li.active > a,\n.tabs-left > .nav-tabs > li.active > a:focus,\n.tabs-left > .nav-tabs > li.active > a:hover {\n  border-color: #C5D0DC transparent #C5D0DC #C5D0DC;\n  border-top-width: 1px;\n  border-left: 2px solid #4C8FBD;\n  margin: 0 -1px;\n  box-shadow: -2px 0 3px 0 rgba(0, 0, 0, 0.15) !important; }\n.tabs-right > .nav-tabs {\n  top: auto;\n  margin-bottom: 0;\n  border-color: #C5D0DC;\n  float: right; }\n.tabs-right > .nav-tabs > li {\n  float: none !important; }\n.tabs-right > .nav-tabs > li > a,\n.tabs-right > .nav-tabs > li > a:focus,\n.tabs-right > .nav-tabs > li > a:hover {\n  border-color: #C5D0DC;\n  margin: 0 -1px; }\n.tabs-right > .nav-tabs > li.active > a,\n.tabs-right > .nav-tabs > li.active > a:focus,\n.tabs-right > .nav-tabs > li.active > a:hover {\n  border-color: #C5D0DC #C5D0DC #C5D0DC transparent;\n  border-top-width: 1px;\n  border-right: 2px solid #4C8FBD;\n  margin: 0 -2px 0 -1px;\n  box-shadow: 2px 0 3px 0 rgba(0, 0, 0, 0.15); }\n.nav-tabs > li > a .badge {\n  padding: 1px 5px;\n  line-height: 15px;\n  opacity: .75;\n  vertical-align: initial; }\n.nav-tabs > li > a .ace-icon {\n  opacity: .75; }\n.nav-tabs > li.active > a .ace-icon,\n.nav-tabs > li.active > a .badge {\n  opacity: 1; }\n.nav-tabs li .ace-icon {\n  width: 1.25em;\n  display: inline-block;\n  text-align: center; }\n.nav-tabs > li.open .dropdown-toggle {\n  background-color: #4F99C6;\n  border-color: #4F99C6;\n  color: #FFF; }\n.nav-tabs > li.open .dropdown-toggle > .ace-icon {\n  color: #FFF !important; }\n.tabs-left .tab-content,\n.tabs-right .tab-content {\n  overflow: auto; }\n.dark {\n  color: #333 !important; }\n.white {\n  color: #FFF !important; }\n.red {\n  color: #DD5A43 !important; }\n.red2 {\n  color: #E08374 !important; }\n.light-red {\n  color: #F77 !important; }\n.blue {\n  color: #478FCA !important; }\n.light-blue {\n  color: #93CBF9 !important; }\n.green {\n  color: #69AA46 !important; }\n.light-green {\n  color: #B0D877 !important; }\n.orange {\n  color: #FF892A !important; }\n.orange2 {\n  color: #FEB902 !important; }\n.light-orange {\n  color: #FCAC6F !important; }\n.purple {\n  color: #A069C3 !important; }\n.pink {\n  color: #C6699F !important; }\n.pink2 {\n  color: #D6487E !important; }\n.brown {\n  color: brown !important; }\n.grey {\n  color: #777 !important; }\n.btn-space {\n  margin-right: 5px; }\n"

/***/ }),

/***/ "./src/app/dashboard/home/home.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/home/home.component.ts ***!
  \**************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _dashboard_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../dashboard.service */ "./src/app/dashboard/dashboard.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = /** @class */ (function () {
    function HomeComponent(dashboardService) {
        this.dashboardService = dashboardService;
        this.userDetails = {
            userName: '',
            firstName: '',
            lastName: '',
            location: ''
        };
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.errors = '';
        this.dashboardService.getHomeDetails().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (response) { return response.json(); }))
            .subscribe(function (result) {
            _this.userDetails.userName = result.email;
            _this.userDetails.firstName = result.firstName;
            _this.userDetails.lastName = result.lastName;
            _this.userDetails.location = result.location;
        }, function (error) { return _this.errors = error; });
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/dashboard/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/dashboard/home/home.component.scss")]
        }),
        __metadata("design:paramtypes", [_dashboard_service__WEBPACK_IMPORTED_MODULE_1__["DashboardService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/management/management.component.html":
/*!****************************************************************!*\
  !*** ./src/app/dashboard/management/management.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <router-outlet></router-outlet>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/dashboard/management/management.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/dashboard/management/management.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/dashboard/management/management.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/dashboard/management/management.component.ts ***!
  \**************************************************************/
/*! exports provided: ManagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagementComponent", function() { return ManagementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ManagementComponent = /** @class */ (function () {
    function ManagementComponent() {
    }
    ManagementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-management',
            template: __webpack_require__(/*! ./management.component.html */ "./src/app/dashboard/management/management.component.html"),
            styles: [__webpack_require__(/*! ./management.component.scss */ "./src/app/dashboard/management/management.component.scss")]
        })
    ], ManagementComponent);
    return ManagementComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/management/user-list/user-list.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/dashboard/management/user-list/user-list.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <div class=\"container\">\r\n    <div class=\"text-center\">\r\n      <h1>User Management</h1>\r\n      <div>\r\n        <table class=\"table table-striped\">\r\n          <thead>\r\n            <tr><td><button class=\"btn\" href=\"#\" routerLinkActive=\"active\" routerLink=\"/dashboard/management/create\">Create User</button></td></tr>\r\n            <tr>\r\n              <th>Firstname</th>\r\n              <th>Lastname</th>\r\n              <th>Email</th>\r\n              <th>Action</th>\r\n            </tr>\r\n          </thead>\r\n          <tbody>\r\n            <tr *ngFor=\"let item of pagedItems, index as i\">\r\n              <td>{{item.firstName}}</td>\r\n              <td>{{item.lastName}}</td>\r\n              <td>{{item.email}}</td>\r\n              <td>\r\n                <a class=\"btn btn-primary btn-space\" (click)=\"getUserDetail(i)\">Detail</a>\r\n                <a class=\"btn btn-primary btn-space\" (click)=\"editUser(i)\">Edit</a>\r\n                <a class=\"btn btn-danger btn-space\" (click)=\"deleteUser(i)\">Delete</a>\r\n              </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n\r\n        <ul *ngIf=\"pager.pages && pager.pages.length\" class=\"pagination\">\r\n          <li [ngClass]=\"{disabled:pager.currentPage === 1}\">\r\n            <a (click)=\"setPage(1)\">First</a>\r\n          </li>\r\n          <li [ngClass]=\"{disabled:pager.currentPage === 1}\">\r\n            <a (click)=\"setPage(pager.currentPage - 1)\">Previous</a>\r\n          </li>\r\n          <li *ngFor=\"let page of pager.pages\" [ngClass]=\"{active:pager.currentPage === page}\">\r\n            <a (click)=\"setPage(page)\">{{page}}</a>\r\n          </li>\r\n          <li [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\r\n            <a (click)=\"setPage(pager.currentPage + 1)\">Next</a>\r\n          </li>\r\n          <li [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\r\n            <a (click)=\"setPage(pager.totalPages)\">Last</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/dashboard/management/user-list/user-list.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/dashboard/management/user-list/user-list.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* pagination */\n.pagination {\n  height: 36px;\n  margin: 18px 0;\n  color: #6c58bF; }\n.pagination ul {\n  display: inline-block;\n  *display: inline;\n  /* IE7 inline-block hack */\n  *zoom: 1;\n  margin-left: 0;\n  color: #ffffff;\n  margin-bottom: 0;\n  border-radius: 3px;\n  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05); }\n.pagination li {\n  display: inline;\n  color: #6c58bF; }\n.pagination a {\n  float: left;\n  padding: 0 14px;\n  line-height: 34px;\n  color: #6c58bF;\n  text-decoration: none;\n  border: 1px solid #ddd;\n  border-left-width: 0; }\n.pagination a:hover,\n.pagination .active a {\n  background-color: #6c58bF;\n  color: #ffffff; }\n.pagination a:focus {\n  background-color: #6c58bF;\n  color: #ffffff; }\n.pagination .active a {\n  color: #ffffff;\n  cursor: default; }\n.pagination .disabled span,\n.pagination .disabled a,\n.pagination .disabled a:hover {\n  color: #999999;\n  background-color: transparent;\n  cursor: default; }\n.pagination li:first-child a {\n  border-left-width: 1px;\n  border-radius: 3px 0 0 3px; }\n.pagination li:last-child a {\n  border-radius: 0 3px 3px 0; }\n.pagination-centered {\n  text-align: center; }\n.pagination-right {\n  text-align: right; }\n.pager {\n  margin-left: 0;\n  margin-bottom: 18px;\n  list-style: none;\n  text-align: center;\n  color: #6c58bF;\n  *zoom: 1; }\n.pager:before,\n.pager:after {\n  display: table;\n  content: \"\"; }\n.center {\n  margin: auto;\n  width: 60%;\n  border: 3px solid #73AD21;\n  padding: 10px; }\n.btn-space {\n  margin-right: 5px; }\n.classWithPad {\n  margin: 10px;\n  padding: 10px; }\n"

/***/ }),

/***/ "./src/app/dashboard/management/user-list/user-list.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/dashboard/management/user-list/user-list.component.ts ***!
  \***********************************************************************/
/*! exports provided: UserListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserListComponent", function() { return UserListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_share_services_pager_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/share/services/pager.service */ "./src/app/share/services/pager.service.ts");
/* harmony import */ var src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/share/services/user.service */ "./src/app/share/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/internal/operators/map */ "./node_modules/rxjs/internal/operators/map.js");
/* harmony import */ var rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UserListComponent = /** @class */ (function () {
    function UserListComponent(http, pagerService, userService, router) {
        this.http = http;
        this.pagerService = pagerService;
        this.userService = userService;
        this.router = router;
        this.user = { firstName: '', lastName: '', email: '', password: '', location: '', role: '' };
        // pager object
        this.pager = {};
    }
    UserListComponent.prototype.ngOnInit = function () {
        // initialize to page 1
        this.setPage(1);
    };
    UserListComponent.prototype.setPage = function (page) {
        var _this = this;
        this.pagerService.getList(page).pipe(Object(rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_5__["map"])(function (response) { return response.json(); })).subscribe(function (jsonObject) {
            // get current page of items
            _this.pagedItems = jsonObject.data;
            // get pager object from service
            _this.pager = _this.pagerService.getPager(jsonObject.totalPages, page);
        });
    };
    UserListComponent.prototype.deleteUser = function (i) {
        var _this = this;
        this.user.email = this.pagedItems[i].email;
        console.log(this.user.email);
        console.log(this.userService.DecodeToken().sub);
        if (this.user.email == this.userService.DecodeToken().sub) {
            this.user.email = "";
        }
        this.userService.deleteUser(this.user.email).subscribe(function (success) { _this.setPage(1); });
    };
    UserListComponent.prototype.editUser = function (i) {
        this.user.email = this.pagedItems[i].email;
        this.router.navigate(['/dashboard/management/edit'], { queryParams: { email: this.user.email } });
    };
    UserListComponent.prototype.getUserDetail = function (i) {
        this.user.email = this.pagedItems[i].email;
        this.router.navigate(['/dashboard/management/detail'], { queryParams: { email: this.user.email } });
    };
    UserListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-list',
            template: __webpack_require__(/*! ./user-list.component.html */ "./src/app/dashboard/management/user-list/user-list.component.html"),
            styles: [__webpack_require__(/*! ./user-list.component.scss */ "./src/app/dashboard/management/user-list/user-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"], src_app_share_services_pager_service__WEBPACK_IMPORTED_MODULE_2__["PagerService"], src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], UserListComponent);
    return UserListComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/root/root.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/root/root.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <nav class=\"col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar\">\r\n    <ul class=\"nav nav-pills flex-column\">\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" href=\"#\" routerLinkActive=\"active\" routerLink=\"/dashboard/home\">Home</a>\r\n      </li>\r\n      <li class=\"nav-item\" *ngIf=\"isAdmin\">\r\n        <a class=\"nav-link\" href=\"#\" routerLinkActive=\"active\" routerLink=\"/dashboard/management\">User Management</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" href=\"#\" routerLinkActive=\"active\" routerLink=\"/dashboard/settings\">Settings</a>\r\n      </li>\r\n    </ul>\r\n  </nav>\r\n  <main role=\"main\" class=\"col-sm-9 ml-sm-auto col-md-10 pt-3\">\r\n    <router-outlet></router-outlet>\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/dashboard/root/root.component.scss":
/*!****************************************************!*\
  !*** ./src/app/dashboard/root/root.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*\r\n * Sidebar\r\n */\n.sidebar {\n  position: fixed;\n  top: 51px;\n  bottom: 0;\n  left: 0;\n  z-index: 1000;\n  padding: 20px 0;\n  overflow-x: hidden;\n  overflow-y: auto;\n  /* Scrollable contents if viewport is shorter than content. */\n  border-right: 1px solid #eee; }\n.sidebar .nav {\n  margin-bottom: 20px; }\n.sidebar .nav-item {\n  width: 100%; }\n.sidebar .nav-item + .nav-item {\n  margin-left: 0; }\n.sidebar .nav-link {\n  border-radius: 0; }\n"

/***/ }),

/***/ "./src/app/dashboard/root/root.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/root/root.component.ts ***!
  \**************************************************/
/*! exports provided: RootComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RootComponent", function() { return RootComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/share/services/user.service */ "./src/app/share/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RootComponent = /** @class */ (function () {
    function RootComponent(user) {
        this.user = user;
    }
    RootComponent.prototype.ngOnInit = function () {
        if (this.user.getRole() == "Admin")
            this.isAdmin = true;
        else
            this.isAdmin = false;
    };
    RootComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./root.component.html */ "./src/app/dashboard/root/root.component.html"),
            styles: [__webpack_require__(/*! ./root.component.scss */ "./src/app/dashboard/root/root.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]])
    ], RootComponent);
    return RootComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/settings/settings.component.html":
/*!************************************************************!*\
  !*** ./src/app/dashboard/settings/settings.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>settings</div>\r\n"

/***/ }),

/***/ "./src/app/dashboard/settings/settings.component.scss":
/*!************************************************************!*\
  !*** ./src/app/dashboard/settings/settings.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/dashboard/settings/settings.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/dashboard/settings/settings.component.ts ***!
  \**********************************************************/
/*! exports provided: SettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsComponent", function() { return SettingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SettingsComponent = /** @class */ (function () {
    function SettingsComponent() {
    }
    SettingsComponent.prototype.ngOnInit = function () {
    };
    SettingsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-settings',
            template: __webpack_require__(/*! ./settings.component.html */ "./src/app/dashboard/settings/settings.component.html"),
            styles: [__webpack_require__(/*! ./settings.component.scss */ "./src/app/dashboard/settings/settings.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SettingsComponent);
    return SettingsComponent;
}());



/***/ }),

/***/ "./src/app/directives/email.validator.directive.ts":
/*!*********************************************************!*\
  !*** ./src/app/directives/email.validator.directive.ts ***!
  \*********************************************************/
/*! exports provided: EmailValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailValidator", function() { return EmailValidator; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


function validateEmailFactory() {
    return function (c) {
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        return EMAIL_REGEXP.test(c.value) ? null : {
            validateEmail: {
                valid: false
            }
        };
    };
}
var EmailValidator = /** @class */ (function () {
    function EmailValidator() {
        this.validator = validateEmailFactory();
    }
    EmailValidator_1 = EmailValidator;
    EmailValidator.prototype.validate = function (c) {
        return this.validator(c);
    };
    EmailValidator = EmailValidator_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[validateEmail][ngModel],[validateEmail][formControl]',
            providers: [
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALIDATORS"], useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return EmailValidator_1; }), multi: true }
            ]
        }),
        __metadata("design:paramtypes", [])
    ], EmailValidator);
    return EmailValidator;
    var EmailValidator_1;
}());



/***/ }),

/***/ "./src/app/directives/focus.directive.ts":
/*!***********************************************!*\
  !*** ./src/app/directives/focus.directive.ts ***!
  \***********************************************/
/*! exports provided: myFocus */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "myFocus", function() { return myFocus; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var myFocus = /** @class */ (function () {
    function myFocus(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        // focus won't work at construction time - too early
    }
    myFocus.prototype.ngOnInit = function () {
        this.renderer.invokeElementMethod(this.el.nativeElement, 'focus', []);
    };
    myFocus = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({ selector: '[tmFocus]' }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer"]])
    ], myFocus);
    return myFocus;
}());



/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>\r\n  <nav class=\"navbar navbar-expand-md navbar-dark fixed-top bg-dark\">\r\n    <a class=\"navbar-brand\" href=\"#\">JwtAuthDemo</a>\r\n    <button class=\"navbar-toggler d-lg-none\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExampleDefault\" aria-controls=\"navbarsExampleDefault\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n      <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n    <div class=\"collapse navbar-collapse\" id=\"navbarsExampleDefault\">\r\n\r\n      <ul *ngIf=\"status\" class=\"navbar-nav mr-auto\">\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" (click)=\"logout()\" href=\"#\">Logoff</a>\r\n        </li>\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" routerLinkActive=\"active\" routerLink=\"/dashboard\">Dashboard</a>\r\n        </li>\r\n      </ul>\r\n      <ul *ngIf=\"!status\" class=\"nav navbar-nav\">\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" routerLinkActive=\"active\" routerLink=\"/register\">Email signup</a>\r\n        </li>\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" routerLinkActive=\"active\" routerLink=\"/login\">Email login</a>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </nav>\r\n</header>\r\n"

/***/ }),

/***/ "./src/app/header/header.component.scss":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/share/services/user.service */ "./src/app/share/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(userService) {
        this.userService = userService;
    }
    HeaderComponent.prototype.logout = function () {
        this.userService.logout();
    };
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.userService.authNavStatus$.subscribe(function (status) { return _this.status = status; });
    };
    HeaderComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_share_services_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<main role=\"main\">\r\n  <div class=\"jumbotron\">\r\n    <div class=\"container\">\r\n      <h1 class=\"display-3\">JWT Auth Demo</h1>\r\n      <p>Hello world!!!!</p>\r\n    </div>\r\n  </div>\r\n</main>\r\n"

/***/ }),

/***/ "./src/app/home/home.component.scss":
/*!******************************************!*\
  !*** ./src/app/home/home.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/home/home.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/role-guard.ts":
/*!*******************************!*\
  !*** ./src/app/role-guard.ts ***!
  \*******************************/
/*! exports provided: RoleGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleGuard", function() { return RoleGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_auth_guard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/auth-guard */ "./src/app/auth-guard.ts");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RoleGuard = /** @class */ (function () {
    function RoleGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    RoleGuard.prototype.canActivate = function (route) {
        var expectedRole = route.data.expectedRole;
        var token = localStorage.getItem('auth_token');
        var tokenPayload = jwt_decode__WEBPACK_IMPORTED_MODULE_3___default()(token);
        if (!this.auth.canActivate() || tokenPayload.Role != expectedRole) {
            this.router.navigate(['/dashboard/home']);
            return false;
        }
        return true;
    };
    RoleGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [src_app_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], RoleGuard);
    return RoleGuard;
}());



/***/ }),

/***/ "./src/app/share/services/base.service.ts":
/*!************************************************!*\
  !*** ./src/app/share/services/base.service.ts ***!
  \************************************************/
/*! exports provided: BaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseService", function() { return BaseService; });
var BaseService = /** @class */ (function () {
    function BaseService() {
    }
    BaseService.prototype.handleError = function (error) {
        var applicationError = error.headers.get('Application-Error');
        if (applicationError) {
            return applicationError;
        }
        var modelStateErrors = '';
        var serverError = error.json();
        if (!serverError.type) {
            for (var key in serverError) {
                if (serverError[key])
                    modelStateErrors += serverError[key] + '\n';
            }
        }
        modelStateErrors = modelStateErrors = '' ? undefined : modelStateErrors;
        return (modelStateErrors || 'Server error');
    };
    return BaseService;
}());



/***/ }),

/***/ "./src/app/share/services/config.service.ts":
/*!**************************************************!*\
  !*** ./src/app/share/services/config.service.ts ***!
  \**************************************************/
/*! exports provided: ConfigService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigService", function() { return ConfigService; });
var ConfigService = /** @class */ (function () {
    function ConfigService() {
        this._apiURI = 'http://localhost:51585/api';
    }
    ConfigService.prototype.getApiURI = function () {
        return this._apiURI;
    };
    return ConfigService;
}());



/***/ }),

/***/ "./src/app/share/services/pager.service.ts":
/*!*************************************************!*\
  !*** ./src/app/share/services/pager.service.ts ***!
  \*************************************************/
/*! exports provided: PagerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagerService", function() { return PagerService; });
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_share_services_config_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/share/services/config.service */ "./src/app/share/services/config.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PagerService = /** @class */ (function () {
    function PagerService(http, configService) {
        this.http = http;
        this.configService = configService;
        this.baseUrl = '';
        this.baseUrl = configService.getApiURI();
    }
    PagerService.prototype.getList = function (page) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_0__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var authToken = localStorage.getItem('auth_token');
        headers.append('Authorization', "Bearer " + authToken);
        return this.http.get(this.baseUrl + "/accounts/getUserList?pageNumber=" + page, { headers: headers });
    };
    PagerService.prototype.getPager = function (totalPages, currentPage) {
        if (currentPage === void 0) { currentPage = 1; }
        // ensure current page isn't out of range
        if (currentPage < 1) {
            currentPage = 1;
        }
        else if (currentPage > totalPages) {
            currentPage = totalPages;
        }
        var startPage, endPage;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        }
        else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            }
            else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            }
            else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }
        // create an array of pages to ng-repeat in the pager control
        var pages = Array.from(Array((endPage + 1) - startPage).keys()).map(function (i) { return startPage + i; });
        // return object with all pager properties required by the view
        return {
            currentPage: currentPage,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            pages: pages
        };
    };
    PagerService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_0__["Http"], src_app_share_services_config_service__WEBPACK_IMPORTED_MODULE_1__["ConfigService"]])
    ], PagerService);
    return PagerService;
}());



/***/ }),

/***/ "./src/app/share/services/user.service.ts":
/*!************************************************!*\
  !*** ./src/app/share/services/user.service.ts ***!
  \************************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./base.service */ "./src/app/share/services/base.service.ts");
/* harmony import */ var _config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./config.service */ "./src/app/share/services/config.service.ts");
/* harmony import */ var rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/internal/BehaviorSubject */ "./node_modules/rxjs/internal/BehaviorSubject.js");
/* harmony import */ var rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UserService = /** @class */ (function (_super) {
    __extends(UserService, _super);
    //
    function UserService(http, configService) {
        var _this = _super.call(this) || this;
        _this.http = http;
        _this.configService = configService;
        _this.baseUrl = '';
        // Observable navItem source
        _this._authNavStatusSource = new rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](false);
        // Observable navItem stream
        _this.authNavStatus$ = _this._authNavStatusSource.asObservable();
        _this.loggedIn = false;
        _this.loggedIn = !!localStorage.getItem('auth_token');
        _this.role = "customer";
        _this._authNavStatusSource.next(_this.loggedIn);
        _this.baseUrl = configService.getApiURI();
        return _this;
    }
    //
    UserService.prototype.register = function (email, password, firstName, lastName, location, role) {
        var body = JSON.stringify({ email: email, password: password, firstName: firstName, lastName: lastName, location: location, role: role });
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]({ 'Content-Type': 'application/json' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["RequestOptions"]({ headers: headers });
        return this.http.post(this.baseUrl + "/accounts/registration", body, options);
    };
    //
    UserService.prototype.createUser = function (email, password, firstName, role, lastName, location) {
        var body = JSON.stringify({ email: email, password: password, firstName: firstName, lastName: lastName, role: role, location: location });
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]();
        headers = this.AddTokenToHeaders();
        return this.http.post(this.baseUrl + "/accounts/createUser", body, { headers: headers });
    };
    //
    UserService.prototype.login = function (userName, password) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.baseUrl + '/auth/login', JSON.stringify({ userName: userName, password: password }), { headers: headers });
    };
    //
    UserService.prototype.getDetail = function (email) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]();
        headers = this.AddTokenToHeaders();
        return this.http.get(this.baseUrl + '/accounts/getDetail?userName=' + email, { headers: headers });
    };
    UserService.prototype.Update = function (email, firstName, lastName) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]();
        headers = this.AddTokenToHeaders();
        return this.http.put(this.baseUrl + '/accounts/update', JSON.stringify({ email: email, firstName: firstName, lastName: lastName }), { headers: headers });
    };
    UserService.prototype.deleteUser = function (userName) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]();
        headers = this.AddTokenToHeaders();
        return this.http.delete(this.baseUrl + '/accounts/delete?userName=' + userName, { headers: headers });
    };
    UserService.prototype.logout = function () {
        localStorage.removeItem('auth_token');
        this.loggedIn = false;
        this._authNavStatusSource.next(false);
    };
    UserService.prototype.isLoggedIn = function () {
        return this.loggedIn;
    };
    UserService.prototype.AddTokenToHeaders = function () {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var authToken = localStorage.getItem('auth_token');
        headers.append('Authorization', "Bearer " + authToken);
        return headers;
    };
    UserService.prototype.setLoggedIn = function () {
        this.loggedIn = true;
    };
    UserService.prototype.getRole = function () {
        return this.DecodeToken().Role;
    };
    UserService.prototype.setauthNavStatus = function () {
        this._authNavStatusSource.next(true);
    };
    UserService.prototype.DecodeToken = function () {
        var token = localStorage.getItem('auth_token');
        var tokenPayload = jwt_decode__WEBPACK_IMPORTED_MODULE_1___default()(token);
        return tokenPayload;
    };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_5__["Http"], _config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]])
    ], UserService);
    return UserService;
}(_base_service__WEBPACK_IMPORTED_MODULE_2__["BaseService"]));



/***/ }),

/***/ "./src/app/share/share.module.ts":
/*!***************************************!*\
  !*** ./src/app/share/share.module.ts ***!
  \***************************************/
/*! exports provided: ShareModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareModule", function() { return ShareModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _directives_focus_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../directives/focus.directive */ "./src/app/directives/focus.directive.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ShareModule = /** @class */ (function () {
    function ShareModule() {
    }
    ShareModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
            declarations: [_directives_focus_directive__WEBPACK_IMPORTED_MODULE_2__["myFocus"]],
            exports: [_directives_focus_directive__WEBPACK_IMPORTED_MODULE_2__["myFocus"]],
            providers: []
        })
    ], ShareModule);
    return ShareModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\sweet_\Documents\New folder\assignment3\LoginSPA\LoginSPA\Client-App\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map