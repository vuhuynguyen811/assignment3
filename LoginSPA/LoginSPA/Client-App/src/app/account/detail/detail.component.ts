import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/share/services/user.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  submitted: boolean = false;
  errors: string;
  private subscription: Subscription;

  public User: any = { email: '', firstName: '', lastName: '' , location: ''};

  constructor(private activatedRoute: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.queryParams.subscribe(
      (param: any) => {
        this.User.email = param['email'];
      });
    this.getUserDetail();
  }

  getUserDetail() {
    this.errors = '';

    this.userService.getDetail(this.User.email).pipe(map(res => res.json()))
      .subscribe(result => {
          this.User.firstName = result.firstName;
          this.User.lastName = result.lastName;
          this.User.location = result.location;
          this.User.email = result.email;
        },
        errors => this.errors = errors);
    }
   
}
