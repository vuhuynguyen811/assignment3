import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/share/services/user.service';
import { RoleGuard } from 'src/app/role-guard';
import { ActivatedRouteSnapshot } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss']
})
export class RootComponent implements OnInit {
  isAdmin: boolean;

  constructor(private user: UserService) { }

  ngOnInit() {
    if (this.user.getRole() == "Admin")
      this.isAdmin = true;
    else
      this.isAdmin = false;
  }

}
