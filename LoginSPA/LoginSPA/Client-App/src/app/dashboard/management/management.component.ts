import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { PagerService } from 'src/app/share/services/pager.service';
import { UserService } from 'src/app/share/services/user.service';
import { map } from 'rxjs/operators';
import { UserViewModel } from 'src/app/share/models/registration-form';
import { Router } from '@angular/router';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss']
})
export class ManagementComponent {

}

