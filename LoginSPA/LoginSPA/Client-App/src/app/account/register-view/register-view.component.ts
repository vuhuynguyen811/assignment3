import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserViewModel } from '../../share/models/registration-form';
import { UserService } from '../../share/services/user.service';
import { finalize, map } from 'rxjs/operators'
import { catchError } from 'rxjs/internal/operators/catchError';

@Component({
  selector: 'app-register-view',
  templateUrl: './register-view.component.html',
  styleUrls: ['./register-view.component.scss']
})
export class RegisterViewComponent implements OnInit {
  errors: string;
  submitted: boolean = false;
  role: string = "customer";

  constructor(private userService: UserService, private router: Router) {

  }

  ngOnInit() {
  }

  registerUser({ value, valid }: { value: UserViewModel, valid: boolean }) {
    this.submitted = true;
    this.errors = '';

    if (valid) {
      this.userService.register(value.email, value.password, value.firstName, value.lastName, value.location, this.role)
        .subscribe(
          result => {
            if (result) {
              this.router.navigate(['/login'], { queryParams: { brandNew: true, email: value.email } });
            }
        },
        errors => { this.errors = this.userService.handleError(errors) });
    }
  }  
}
