import { Component, OnInit } from '@angular/core';
import { UserViewModel } from 'src/app/share/models/registration-form';
import { Http } from '@angular/http';
import { PagerService } from 'src/app/share/services/pager.service';
import { UserService } from 'src/app/share/services/user.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/internal/operators/map';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  errors: string;

  private user: UserViewModel = { firstName: '', lastName: '', email: '', password: '', location: '', role: '' };

  constructor(private http: Http, private pagerService: PagerService, private userService: UserService, private router: Router) { }

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  ngOnInit() {
    // initialize to page 1
    this.setPage(1);
  }

  setPage(page) {

    this.pagerService.getList(page).pipe(map(response => response.json())).subscribe(jsonObject => {
      // get current page of items
      this.pagedItems = jsonObject.data;
      // get pager object from service
      this.pager = this.pagerService.getPager(jsonObject.totalPages, page);
    });
  }

  deleteUser(i) {
    this.user.email = this.pagedItems[i].email;

    console.log(this.user.email);
    console.log(this.userService.DecodeToken().sub);
    if (this.user.email == this.userService.DecodeToken().sub) {
        this.user.email = "";
    }
    this.userService.deleteUser(this.user.email).subscribe(success => { this.setPage(1) });
  }

  editUser(i) {
    this.user.email = this.pagedItems[i].email;

    this.router.navigate(['/dashboard/management/edit'], { queryParams: { email: this.user.email} });
  }

  getUserDetail(i) {
    this.user.email = this.pagedItems[i].email;

    this.router.navigate(['/dashboard/management/detail'], { queryParams: { email: this.user.email } });
  }

}
