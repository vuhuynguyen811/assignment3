﻿using AutoMapper;
using Login.Models.Entities;
using Login.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Login.Models.Data
{
    public class MappingProfile : Profile
	{
		

		public MappingProfile()
		{
			CreateMap<UserDto, User>().ForMember(au => au.UserName, map => map.MapFrom(vm => vm.Email));
			CreateMap<User, UserDto>().ForMember(au => au.Email, map => map.MapFrom(vm => vm.UserName));
		}
	}
}
